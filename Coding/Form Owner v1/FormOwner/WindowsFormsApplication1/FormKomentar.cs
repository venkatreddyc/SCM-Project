﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace WindowsFormsApplication1
{
    public partial class FormKomentar : Form
    {
        string sDataPass = string.Empty;
        public FormKomentar()
        {
            InitializeComponent();
        }
        public FormKomentar(string sPassing)
        {
            InitializeComponent();
            sDataPass = sPassing;
            label2.Text = sPassing;
        }

        private void FormKomentar_Load(object sender, EventArgs e)
        {
            label4.Text = DateTime.Now.ToString("HH:MM");
            label5.Text = DateTime.Now.ToString("dd/MM/yyyy");
            LUser listUser = new LUser();
            string hasilBaca = listUser.CariData("username", "fleming");
            if (hasilBaca == "sukses")
            {

                label3.Text = "Hallo, " + listUser.ListPegawai[0].Nama;


            }
            else
            {
                MessageBox.Show(hasilBaca);
            }
        }

        public string dariFormVeri
        {
            get { return label2.Text; }
        }

        private void buttonKirimKomentar_Click(object sender, EventArgs e)
        {
            DaftarSpk dSpk = new DaftarSpk();
            string text = textBoxKetik.Text;
            string noSp = label2.Text;
            dSpk.Update(noSp,text);
            string hasil = dSpk.Update(noSp, text);

            if (hasil == "sukses")
            {
                MessageBox.Show("data telah terupdate");
            }
            else
            {
                MessageBox.Show(hasil);
            }
        }

        private void buttonKembali_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}

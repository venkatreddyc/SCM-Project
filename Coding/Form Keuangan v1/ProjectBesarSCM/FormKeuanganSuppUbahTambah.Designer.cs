﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganSuppUbahTambah
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelHargaSatuan = new System.Windows.Forms.Label();
            this.cmbBahanBaku = new System.Windows.Forms.ComboBox();
            this.labelBahanBaku = new System.Windows.Forms.Label();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.labelHello = new System.Windows.Forms.Label();
            this.txtHargaSatuan = new System.Windows.Forms.TextBox();
            this.labelJam = new System.Windows.Forms.Label();
            this.labelWaktu = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelHargaSatuan
            // 
            this.labelHargaSatuan.AutoSize = true;
            this.labelHargaSatuan.Location = new System.Drawing.Point(256, 270);
            this.labelHargaSatuan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHargaSatuan.Name = "labelHargaSatuan";
            this.labelHargaSatuan.Size = new System.Drawing.Size(129, 17);
            this.labelHargaSatuan.TabIndex = 24;
            this.labelHargaSatuan.Text = "HARGA SATUAN : ";
            // 
            // cmbBahanBaku
            // 
            this.cmbBahanBaku.FormattingEnabled = true;
            this.cmbBahanBaku.Location = new System.Drawing.Point(384, 169);
            this.cmbBahanBaku.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBahanBaku.Name = "cmbBahanBaku";
            this.cmbBahanBaku.Size = new System.Drawing.Size(192, 24);
            this.cmbBahanBaku.TabIndex = 23;
            // 
            // labelBahanBaku
            // 
            this.labelBahanBaku.AutoSize = true;
            this.labelBahanBaku.Location = new System.Drawing.Point(273, 172);
            this.labelBahanBaku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelBahanBaku.Name = "labelBahanBaku";
            this.labelBahanBaku.Size = new System.Drawing.Size(108, 17);
            this.labelBahanBaku.TabIndex = 22;
            this.labelBahanBaku.Text = "BAHAN BAKU : ";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Location = new System.Drawing.Point(384, 135);
            this.cmbSupplier.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(192, 24);
            this.cmbSupplier.TabIndex = 21;
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.Location = new System.Drawing.Point(293, 139);
            this.labelSupplier.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(87, 17);
            this.labelSupplier.TabIndex = 20;
            this.labelSupplier.Text = "SUPPLIER : ";
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(533, 15);
            this.labelHello.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(56, 17);
            this.labelHello.TabIndex = 19;
            this.labelHello.Text = "Hello  , ";
            // 
            // txtHargaSatuan
            // 
            this.txtHargaSatuan.Location = new System.Drawing.Point(391, 266);
            this.txtHargaSatuan.Margin = new System.Windows.Forms.Padding(4);
            this.txtHargaSatuan.Name = "txtHargaSatuan";
            this.txtHargaSatuan.Size = new System.Drawing.Size(132, 22);
            this.txtHargaSatuan.TabIndex = 25;
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.Location = new System.Drawing.Point(11, 486);
            this.labelJam.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(34, 17);
            this.labelJam.TabIndex = 27;
            this.labelJam.Text = "Jam";
            // 
            // labelWaktu
            // 
            this.labelWaktu.AutoSize = true;
            this.labelWaktu.Location = new System.Drawing.Point(11, 505);
            this.labelWaktu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelWaktu.Name = "labelWaktu";
            this.labelWaktu.Size = new System.Drawing.Size(48, 17);
            this.labelWaktu.TabIndex = 26;
            this.labelWaktu.Text = "Waktu";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkGray;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(253, 340);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(286, 51);
            this.button1.TabIndex = 28;
            this.button1.Text = "SIMPAN";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(654, 468);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 50);
            this.button2.TabIndex = 29;
            this.button2.Text = "KEMBALI";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FormKeuanganSuppUbahTambah
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Supp_Ubah_Tambah;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(759, 530);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.labelWaktu);
            this.Controls.Add(this.txtHargaSatuan);
            this.Controls.Add(this.labelHargaSatuan);
            this.Controls.Add(this.cmbBahanBaku);
            this.Controls.Add(this.labelBahanBaku);
            this.Controls.Add(this.cmbSupplier);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.labelHello);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormKeuanganSuppUbahTambah";
            this.Text = "FormKeuanganSuppUbahTambah";
            this.Load += new System.EventHandler(this.FormKeuanganSuppUbahTambah_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHargaSatuan;
        private System.Windows.Forms.ComboBox cmbBahanBaku;
        private System.Windows.Forms.Label labelBahanBaku;
        private System.Windows.Forms.ComboBox cmbSupplier;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.Label labelHello;
        private System.Windows.Forms.TextBox txtHargaSatuan;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Label labelWaktu;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}
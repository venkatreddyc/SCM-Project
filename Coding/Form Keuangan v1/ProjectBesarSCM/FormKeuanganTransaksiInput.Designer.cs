﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganTransaksiInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelHello = new System.Windows.Forms.Label();
            this.labelNoSpk = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelInfoSpk = new System.Windows.Forms.Label();
            this.labelInfoTotal = new System.Windows.Forms.Label();
            this.labelPembayaran1 = new System.Windows.Forms.Label();
            this.labelPembayaran2 = new System.Windows.Forms.Label();
            this.textBoxInforPembayaran1 = new System.Windows.Forms.TextBox();
            this.textBoxInfoPembayaran2 = new System.Windows.Forms.TextBox();
            this.comboBoxCaraBayar = new System.Windows.Forms.ComboBox();
            this.comboBoxCaraBayar2 = new System.Windows.Forms.ComboBox();
            this.dateTimePickerPembayaran1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerPembayaran2 = new System.Windows.Forms.DateTimePicker();
            this.labelKekurangan = new System.Windows.Forms.Label();
            this.labelInfoKekurangan = new System.Windows.Forms.Label();
            this.labelJam = new System.Windows.Forms.Label();
            this.labelWaktu = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(486, 16);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(43, 13);
            this.labelHello.TabIndex = 21;
            this.labelHello.Text = "Hello  , ";
            // 
            // labelNoSpk
            // 
            this.labelNoSpk.AutoSize = true;
            this.labelNoSpk.Location = new System.Drawing.Point(100, 110);
            this.labelNoSpk.Name = "labelNoSpk";
            this.labelNoSpk.Size = new System.Drawing.Size(54, 13);
            this.labelNoSpk.TabIndex = 22;
            this.labelNoSpk.Text = "No SPK : ";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(82, 146);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(72, 13);
            this.labelTotal.TabIndex = 23;
            this.labelTotal.Text = "Total Harga : ";
            // 
            // labelInfoSpk
            // 
            this.labelInfoSpk.AutoSize = true;
            this.labelInfoSpk.Location = new System.Drawing.Point(160, 110);
            this.labelInfoSpk.Name = "labelInfoSpk";
            this.labelInfoSpk.Size = new System.Drawing.Size(41, 13);
            this.labelInfoSpk.TabIndex = 24;
            this.labelInfoSpk.Text = "ini SPK";
            // 
            // labelInfoTotal
            // 
            this.labelInfoTotal.AutoSize = true;
            this.labelInfoTotal.Location = new System.Drawing.Point(160, 146);
            this.labelInfoTotal.Name = "labelInfoTotal";
            this.labelInfoTotal.Size = new System.Drawing.Size(50, 13);
            this.labelInfoTotal.TabIndex = 25;
            this.labelInfoTotal.Text = "Ini Harga";
            // 
            // labelPembayaran1
            // 
            this.labelPembayaran1.AutoSize = true;
            this.labelPembayaran1.Location = new System.Drawing.Point(70, 178);
            this.labelPembayaran1.Name = "labelPembayaran1";
            this.labelPembayaran1.Size = new System.Drawing.Size(84, 13);
            this.labelPembayaran1.TabIndex = 26;
            this.labelPembayaran1.Text = "Pembayaran 1 : ";
            // 
            // labelPembayaran2
            // 
            this.labelPembayaran2.AutoSize = true;
            this.labelPembayaran2.Location = new System.Drawing.Point(73, 211);
            this.labelPembayaran2.Name = "labelPembayaran2";
            this.labelPembayaran2.Size = new System.Drawing.Size(81, 13);
            this.labelPembayaran2.TabIndex = 27;
            this.labelPembayaran2.Text = "Pembayaran 2 :";
            // 
            // textBoxInforPembayaran1
            // 
            this.textBoxInforPembayaran1.Location = new System.Drawing.Point(163, 175);
            this.textBoxInforPembayaran1.Name = "textBoxInforPembayaran1";
            this.textBoxInforPembayaran1.Size = new System.Drawing.Size(121, 20);
            this.textBoxInforPembayaran1.TabIndex = 28;
            // 
            // textBoxInfoPembayaran2
            // 
            this.textBoxInfoPembayaran2.Location = new System.Drawing.Point(163, 205);
            this.textBoxInfoPembayaran2.Name = "textBoxInfoPembayaran2";
            this.textBoxInfoPembayaran2.Size = new System.Drawing.Size(121, 20);
            this.textBoxInfoPembayaran2.TabIndex = 29;
            // 
            // comboBoxCaraBayar
            // 
            this.comboBoxCaraBayar.FormattingEnabled = true;
            this.comboBoxCaraBayar.Location = new System.Drawing.Point(290, 175);
            this.comboBoxCaraBayar.Name = "comboBoxCaraBayar";
            this.comboBoxCaraBayar.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCaraBayar.TabIndex = 30;
            // 
            // comboBoxCaraBayar2
            // 
            this.comboBoxCaraBayar2.FormattingEnabled = true;
            this.comboBoxCaraBayar2.Location = new System.Drawing.Point(290, 205);
            this.comboBoxCaraBayar2.Name = "comboBoxCaraBayar2";
            this.comboBoxCaraBayar2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCaraBayar2.TabIndex = 31;
            // 
            // dateTimePickerPembayaran1
            // 
            this.dateTimePickerPembayaran1.Location = new System.Drawing.Point(417, 176);
            this.dateTimePickerPembayaran1.Name = "dateTimePickerPembayaran1";
            this.dateTimePickerPembayaran1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerPembayaran1.TabIndex = 32;
            // 
            // dateTimePickerPembayaran2
            // 
            this.dateTimePickerPembayaran2.Location = new System.Drawing.Point(417, 205);
            this.dateTimePickerPembayaran2.Name = "dateTimePickerPembayaran2";
            this.dateTimePickerPembayaran2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerPembayaran2.TabIndex = 33;
            // 
            // labelKekurangan
            // 
            this.labelKekurangan.AutoSize = true;
            this.labelKekurangan.ForeColor = System.Drawing.Color.Red;
            this.labelKekurangan.Location = new System.Drawing.Point(80, 243);
            this.labelKekurangan.Name = "labelKekurangan";
            this.labelKekurangan.Size = new System.Drawing.Size(74, 13);
            this.labelKekurangan.TabIndex = 34;
            this.labelKekurangan.Text = "Kekurangan : ";
            // 
            // labelInfoKekurangan
            // 
            this.labelInfoKekurangan.AutoSize = true;
            this.labelInfoKekurangan.Location = new System.Drawing.Point(160, 243);
            this.labelInfoKekurangan.Name = "labelInfoKekurangan";
            this.labelInfoKekurangan.Size = new System.Drawing.Size(71, 13);
            this.labelInfoKekurangan.TabIndex = 35;
            this.labelInfoKekurangan.Text = "ini Kurangnya";
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.Location = new System.Drawing.Point(5, 498);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(26, 13);
            this.labelJam.TabIndex = 38;
            this.labelJam.Text = "Jam";
            // 
            // labelWaktu
            // 
            this.labelWaktu.AutoSize = true;
            this.labelWaktu.Location = new System.Drawing.Point(5, 513);
            this.labelWaktu.Name = "labelWaktu";
            this.labelWaktu.Size = new System.Drawing.Size(39, 13);
            this.labelWaktu.TabIndex = 37;
            this.labelWaktu.Text = "Waktu";
            // 
            // FormKeuanganTransaksiInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Transaksi_Input;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(689, 539);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.labelWaktu);
            this.Controls.Add(this.labelInfoKekurangan);
            this.Controls.Add(this.labelKekurangan);
            this.Controls.Add(this.dateTimePickerPembayaran2);
            this.Controls.Add(this.dateTimePickerPembayaran1);
            this.Controls.Add(this.comboBoxCaraBayar2);
            this.Controls.Add(this.comboBoxCaraBayar);
            this.Controls.Add(this.textBoxInfoPembayaran2);
            this.Controls.Add(this.textBoxInforPembayaran1);
            this.Controls.Add(this.labelPembayaran2);
            this.Controls.Add(this.labelPembayaran1);
            this.Controls.Add(this.labelInfoTotal);
            this.Controls.Add(this.labelInfoSpk);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.labelNoSpk);
            this.Controls.Add(this.labelHello);
            this.Name = "FormKeuanganTransaksiInput";
            this.Text = "FormKeuanganTransaksiInput";
            this.Load += new System.EventHandler(this.FormKeuanganTransaksiInput_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHello;
        private System.Windows.Forms.Label labelNoSpk;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelInfoSpk;
        private System.Windows.Forms.Label labelInfoTotal;
        private System.Windows.Forms.Label labelPembayaran1;
        private System.Windows.Forms.Label labelPembayaran2;
        private System.Windows.Forms.TextBox textBoxInforPembayaran1;
        private System.Windows.Forms.TextBox textBoxInfoPembayaran2;
        private System.Windows.Forms.ComboBox comboBoxCaraBayar;
        private System.Windows.Forms.ComboBox comboBoxCaraBayar2;
        private System.Windows.Forms.DateTimePicker dateTimePickerPembayaran1;
        private System.Windows.Forms.DateTimePicker dateTimePickerPembayaran2;
        private System.Windows.Forms.Label labelKekurangan;
        private System.Windows.Forms.Label labelInfoKekurangan;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Label labelWaktu;
    }
}
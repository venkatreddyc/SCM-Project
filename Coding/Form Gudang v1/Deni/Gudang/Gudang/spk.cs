﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Gudang
{
    public class spk
    {
        private List<noSPK> listSPK;

        public List<noSPK> ListSPK
        {
            get { return listSPK; }
            set { listSPK = value; }
        }

        public int JumlahID
        {
            get { return listSPK.Count; }
        }

        public spk()
        {
            listSPK = new List<noSPK>();
        }


        public string bacaSemuaData()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT nospk from spk";

            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while (Data.Read() == true)
                {
                    string kode = Data.GetValue(0).ToString();
                    noSPK spk = new noSPK(kode);
                    listSPK.Add(spk);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
    }
}

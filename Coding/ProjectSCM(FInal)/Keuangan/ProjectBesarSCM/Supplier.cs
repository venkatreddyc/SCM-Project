﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectBesarSCM
{
    public class Supplier
    {
        private string kodeSup;
        private string namaSup;
        private string alamat;

        public string KodeSup
        {
            get { return kodeSup; }
            set { kodeSup = value; }
        }
        public string NamaSup
        {
            get { return namaSup; }
            set { namaSup = value; }
        }
        public string Alamat
        {
            get { return alamat; }
            set { alamat = value; }
        }
        public Supplier()
        {
            kodeSup = "";
            namaSup = "";
            alamat = "";
        }
        public Supplier(string kode, string nama, string add)
        {
            kodeSup = kode;
            namaSup = nama;
            alamat = add;
        }
    }
}

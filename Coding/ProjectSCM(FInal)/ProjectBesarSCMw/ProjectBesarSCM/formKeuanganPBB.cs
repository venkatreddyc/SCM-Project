﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectBesarSCM
{
    public partial class formKeuanganPBB : Form
    {
        public formKeuanganPBB()
        {
            InitializeComponent();
        }

        #region Variable Bantuan & Global
        string kodeBahanbaku;
        string PilihanSupplier;
        int JumlahBeli = 0;
        int hargasatuan = 0;
        int totalhargaGlobal = 0;
        List<string> ListKodeBarang = new List<string>();
        List<string> ListKodeSupplier = new List<string>();
        public List<string> Nama = new List<string>();
        #endregion
        
        private void BersihkanForm()
        {
            comboBoxSupplier.Text = "";
            comboBoxBahanBaku.Text = "";
            
            labelHarga.Text = "0";
            numericUpDownJumlah.Value = 0;
            labelInfoTotalHarga.Text = "0";
            dataGridViewInfo.Rows.Clear();
        }
        private void formKeuanganPBB_Load(object sender, EventArgs e)
        {

            #region Inisialisasi
            FormKeuanganHome FKH = (FormKeuanganHome)this.Owner;
            ListPembelian LP = new ListPembelian();
            ListBahanBaku LB = new ListBahanBaku();
            ListSupplier LS = new ListSupplier();

            //labelHello.Text = "Hello, " + FKH.Nama[0] + "-" + FKH.Nama[0];
            BersihkanForm();
          

            #endregion


            string hasilBacaBahanBaku = "";
            string hasilBacaSupplier = "";
            string hasilBacaKode = "";
           

            #region GenerateCode
            hasilBacaKode = LP.AutoGenerateCode();
            if (hasilBacaKode == "sukses")
            {
                labelInfoKodePembelian.Text = LP.KodePembelian;
            }
            else
            {
                MessageBox.Show("Generate Gagal dengan kode : " + hasilBacaKode);
            }
            #endregion

            #region GenerateBahanBaku
            hasilBacaBahanBaku = LB.BacaSemuaData();
            if (hasilBacaBahanBaku == "sukses")
            {
                comboBoxBahanBaku.Items.Clear();
                for (int i = 0; i < LB.JumlahBarang; i++)
                {
                    comboBoxBahanBaku.Items.Add(LB.LBahanBaku[i].KodeBarang + " - " + LB.LBahanBaku[i].NamaBarang);
                }

            }
            else
            {
                MessageBox.Show("Error, Failed to Load BahanBaku : " + hasilBacaBahanBaku);
            }
            #endregion

            #region GenerateSupplier
            hasilBacaSupplier = LS.BacaSemuaData();
            if (hasilBacaSupplier == "sukses")
            {
                comboBoxSupplier.Items.Clear();
                for (int i = 0; i < LS.JumlahList; i++)
                {
                    comboBoxSupplier.Items.Add(LS.LSupplier[i].KodeSup + " - " + LS.LSupplier[i].NamaSup);
                }

            }
            else
            {
                MessageBox.Show(hasilBacaSupplier);
            }
            #endregion
            FormKeuanganHome F1 = (FormKeuanganHome)this.Owner;
            labelHello.Text = F1.labelHello.Text;
            //Nama.Add(F1.nama);
            //labelHello.Text = "Hello, " + F1.nama + "-" + F1.nama;
           
        }
        
        private void buttonSupplier_Click(object sender, EventArgs e)
        {
            FormKeuanganSupp formKeuanganSupp = new FormKeuanganSupp();
            formKeuanganSupp.Owner = this;
            formKeuanganSupp.Show();
        }

        private void comboBoxSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {

            #region AutoGenerateCariDataBahanBaku
                PilihanSupplier = comboBoxSupplier.SelectedItem.ToString().Substring(0, 2);
                ListDetilBahanBaku LDBB = new ListDetilBahanBaku();
                string hasil1 = LDBB.cariData("kodeSupplier", PilihanSupplier.ToLower(), "kodeBahanBaku", kodeBahanbaku.ToLower());
                if (hasil1 == "sukses")
                {
                    for (int i = 0; i < LDBB.JumlahList; i++ )
                    {
                        hargasatuan = LDBB.LDetilBahanBaku[0].HargaSatuan;
                        labelHarga.Text = LDBB.LDetilBahanBaku[0].HargaSatuan.ToString();
                        labelInfoTotalHarga.Text = (hargasatuan * JumlahBeli).ToString();
                    }

                }
                else
                {
                    MessageBox.Show(hasil1);
                }
            #endregion
          
        }        
        private void comboBoxBahanBaku_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region AutoGenerateCariDataSupplier
                kodeBahanbaku = comboBoxBahanBaku.SelectedItem.ToString().Substring(0, 5);
                if (comboBoxSupplier.SelectedIndex == -1)
                {

                }
                else
                {
                    ListDetilBahanBaku LDBB = new ListDetilBahanBaku();
                    string hasil1 = LDBB.cariData("kodeSupplier", PilihanSupplier.ToLower(), "kodeBahanBaku", kodeBahanbaku.ToLower());
                    if (hasil1 == "sukses")
                    {
                        hargasatuan = LDBB.LDetilBahanBaku[0].HargaSatuan;
                        labelHarga.Text = LDBB.LDetilBahanBaku[0].HargaSatuan.ToString();
                        labelInfoTotalHarga.Text = (hargasatuan * JumlahBeli).ToString();
                    }
                    else
                    {
                        MessageBox.Show(hasil1);
                    }
                }
            #endregion
          
        }

        private void textBoxJumlah_KeyPress(object sender, KeyPressEventArgs e)
        {
            #region Tidak DIPAKAI
            
            #endregion
        }

        private void numericUpDownJumlah_ValueChanged(object sender, EventArgs e)
        {

            #region Calculation
            hargasatuan = int.Parse(labelHarga.Text);
            JumlahBeli = (int)numericUpDownJumlah.Value;
            labelInfoTotalHarga.Text = (hargasatuan * JumlahBeli).ToString();
            #endregion
            
        }

        private void buttonS_Click(object sender, EventArgs e)
        {
            if((int)numericUpDownJumlah.Value == 0 || comboBoxBahanBaku.Text == "" || comboBoxSupplier.Text == "")
            {
                MessageBox.Show("Lengkapi Data yang ada");
                
            }
            else
            {
                string namabahanBaku = comboBoxBahanBaku.Text.Substring(8);
                string kodebahanBaku = comboBoxBahanBaku.Text.Substring(0, 5);

                string kodeSupplier = comboBoxSupplier.Text.Substring(0, 2);
                ListKodeSupplier.Add(kodeSupplier);
                int hargaSatuan = int.Parse(labelHarga.Text);
                int jumlah = (int)numericUpDownJumlah.Value;
                int subtotal = int.Parse(labelInfoTotalHarga.Text);
                int status = 1;
                totalhargaGlobal += subtotal;
                dataGridViewInfo.Rows.Add(namabahanBaku, kodeSupplier, hargasatuan, jumlah, subtotal, status, "11-11-1911");
                ListKodeBarang.Add(kodebahanBaku);
                labelInfoTotal.Text = totalhargaGlobal.ToString();
            }
            
           
        }

        private void buttonBeli_Click(object sender, EventArgs e)
        {
           
            Supplier SS = new Supplier();
            List<PembelianDetil> LPembelian = new List<PembelianDetil>();

            for (int i = 0; i < dataGridViewInfo.Rows.Count-1; i++)
            {
                if(dataGridViewInfo.Rows[i].Cells["Jumlah"].Value.ToString() != "0")
                {
                    BahanBaku BB = new BahanBaku();
                    BB.KodeBarang = ListKodeBarang[i];
                    BB.NamaBarang = dataGridViewInfo.Rows[i].Cells["namaBahan"].Value.ToString();

                    SS.KodeSup = ListKodeSupplier[i];
                    int jumlah = int.Parse(dataGridViewInfo.Rows[i].Cells["Jumlah"].Value.ToString());
                    int subtotal = int.Parse(dataGridViewInfo.Rows[i].Cells["Total"].Value.ToString());
                    int hargaSatuan = int.Parse(dataGridViewInfo.Rows[i].Cells["HargaSatuan"].Value.ToString());
                    int status = int.Parse(dataGridViewInfo.Rows[i].Cells["Status"].Value.ToString());

                    PembelianDetil PD = new PembelianDetil(BB, subtotal, 22, 1, DateTime.Now.ToString("yyyy-MM-dd"), subtotal / hargasatuan);
                    LPembelian.Add(PD);
                }
               
            }
            Pembelian P = new Pembelian(labelInfoKodePembelian.Text,DateTime.Now.ToString("yyyy-MM-dd"), SS, LPembelian);
            ListPembelian LP = new ListPembelian();
            string hasilTambah = LP.AddData(P);
            string hasilBacaKode = "";
            if (hasilTambah == "sukses")
            {
            
               
                MessageBox.Show("Data Telah Tersimpan");

                #region GenerateCode
                hasilBacaKode = LP.AutoGenerateCode();
                if (hasilBacaKode == "sukses")
                {
                    labelInfoKodePembelian.Text = LP.KodePembelian;
                }
                else
                {
                    MessageBox.Show("Generate Gagal dengan kode : " + hasilBacaKode);
                }
                #endregion

                BersihkanForm();

               
            }
            else
            {
                MessageBox.Show("Gagal : " + hasilTambah);
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

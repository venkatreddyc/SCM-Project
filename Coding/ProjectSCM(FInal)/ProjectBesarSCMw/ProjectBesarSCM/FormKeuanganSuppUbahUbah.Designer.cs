﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganSuppUbahUbah
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxHargaSatuan = new System.Windows.Forms.TextBox();
            this.labelHargaSatuan = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.labelBahanBaku = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.labelHello = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelAlamat = new System.Windows.Forms.Label();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonSimpan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxHargaSatuan
            // 
            this.textBoxHargaSatuan.Location = new System.Drawing.Point(337, 214);
            this.textBoxHargaSatuan.Name = "textBoxHargaSatuan";
            this.textBoxHargaSatuan.Size = new System.Drawing.Size(100, 20);
            this.textBoxHargaSatuan.TabIndex = 34;
            // 
            // labelHargaSatuan
            // 
            this.labelHargaSatuan.AutoSize = true;
            this.labelHargaSatuan.Location = new System.Drawing.Point(237, 217);
            this.labelHargaSatuan.Name = "labelHargaSatuan";
            this.labelHargaSatuan.Size = new System.Drawing.Size(101, 13);
            this.labelHargaSatuan.TabIndex = 33;
            this.labelHargaSatuan.Text = "HARGA SATUAN : ";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(337, 156);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(145, 21);
            this.comboBox2.TabIndex = 32;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // labelBahanBaku
            // 
            this.labelBahanBaku.AutoSize = true;
            this.labelBahanBaku.Location = new System.Drawing.Point(254, 159);
            this.labelBahanBaku.Name = "labelBahanBaku";
            this.labelBahanBaku.Size = new System.Drawing.Size(85, 13);
            this.labelBahanBaku.TabIndex = 31;
            this.labelBahanBaku.Text = "BAHAN BAKU : ";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(337, 129);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(145, 21);
            this.comboBox1.TabIndex = 30;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.Location = new System.Drawing.Point(269, 132);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(69, 13);
            this.labelSupplier.TabIndex = 29;
            this.labelSupplier.Text = "SUPPLIER : ";
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(495, 17);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(43, 13);
            this.labelHello.TabIndex = 28;
            this.labelHello.Text = "Hello  , ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(337, 186);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 38;
            // 
            // labelAlamat
            // 
            this.labelAlamat.AutoSize = true;
            this.labelAlamat.Location = new System.Drawing.Point(279, 189);
            this.labelAlamat.Name = "labelAlamat";
            this.labelAlamat.Size = new System.Drawing.Size(59, 13);
            this.labelAlamat.TabIndex = 37;
            this.labelAlamat.Text = "ALAMAT : ";
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.BorderSize = 0;
            this.buttonLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Location = new System.Drawing.Point(608, 494);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(88, 40);
            this.buttonLogin.TabIndex = 40;
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonSimpan
            // 
            this.buttonSimpan.BackColor = System.Drawing.Color.Transparent;
            this.buttonSimpan.FlatAppearance.BorderSize = 0;
            this.buttonSimpan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonSimpan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSimpan.Location = new System.Drawing.Point(240, 359);
            this.buttonSimpan.Name = "buttonSimpan";
            this.buttonSimpan.Size = new System.Drawing.Size(251, 46);
            this.buttonSimpan.TabIndex = 39;
            this.buttonSimpan.UseVisualStyleBackColor = false;
            this.buttonSimpan.Click += new System.EventHandler(this.buttonSimpan_Click);
            // 
            // FormKeuanganSuppUbahUbah
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Supp_Ubah_Ubah;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(701, 554);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.buttonSimpan);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.labelAlamat);
            this.Controls.Add(this.textBoxHargaSatuan);
            this.Controls.Add(this.labelHargaSatuan);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.labelBahanBaku);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.labelHello);
            this.Name = "FormKeuanganSuppUbahUbah";
            this.Text = "FormKeuanganSuppUbahUbah";
            this.Load += new System.EventHandler(this.FormKeuanganSuppUbahUbah_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxHargaSatuan;
        private System.Windows.Forms.Label labelHargaSatuan;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label labelBahanBaku;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label labelAlamat;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonSimpan;
        public System.Windows.Forms.Label labelHello;
    }
}
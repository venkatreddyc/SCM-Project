﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
namespace ProjectBesarSCM
{
    class ListDetilBahanBaku
    {
        private List<DetilBahanBaku> listDetilBahanBaku;
        private int kode;
        private string nomer;
        public List<DetilBahanBaku> LDetilBahanBaku
        {
            get { return listDetilBahanBaku; }
        }
        public int Kode
        {
            get { return kode; }
        }
        public int JumlahList
        {
            get { return listDetilBahanBaku.Count; }
        }
        public string Nomer
        {
            get { return nomer; }
        }

        public ListDetilBahanBaku()
        {
            listDetilBahanBaku = new List<DetilBahanBaku>();
            kode = 1;
            nomer = "01";
        }


        public string BacaSemuaData()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT bb.KodeBahanBaku, bb.nama, bb.satuan, bb.stok, s.kodeSupplier, s.namaSupplier, s.alamat, DBB.hargasatuan from detilbahanbakuSupplier DBB inner join BahanBaku BB ON DBB.kodeBahanBaku = BB.KodeBahanBaku INNER JOIN Supplier s ON DBB.kodesupplier = s.kodesupplier";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while(Data.Read() == true)
                {
                    string KodeBahan = Data.GetValue(0).ToString();
                    string namaBahan = Data.GetValue(1).ToString();
                    string satuanBahan = Data.GetValue(2).ToString();
                    int stockBahan = int.Parse(Data.GetValue(3).ToString());

                    string kodeSupplier = Data.GetValue(4).ToString();
                    string namaSupplier = Data.GetValue(5).ToString();
                    string alamatSupplier = Data.GetValue(6).ToString();

                    int HargaSatuan = int.Parse(Data.GetValue(7).ToString());

                    BahanBaku BB = new BahanBaku(KodeBahan, namaBahan, satuanBahan, stockBahan);
                    Supplier SS = new Supplier(kodeSupplier, namaSupplier, alamatSupplier);
                    DetilBahanBaku DBB = new DetilBahanBaku(BB, SS, HargaSatuan);
                    listDetilBahanBaku.Add(DBB);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";

            }
            catch(Exception E)
            {
                return E.Message;
            }
        }
        public string cariData(string kriteria1, string nilai1, string kriteria2, string nilai2)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT bb.KodeBahanBaku, bb.nama, bb.satuan, bb.stok, s.kodeSupplier, s.namaSupplier, s.alamat, DBB.hargasatuan from detilbahanbakuSupplier DBB inner join BahanBaku BB ON DBB.kodeBahanBaku = BB.KodeBahanBaku INNER JOIN Supplier s ON DBB.kodesupplier = s.kodesupplier where s." + kriteria1 + " = '" + nilai1 + "' and BB." + kriteria2 + " = '" + nilai2 + "'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                while(Data.Read() == true)
                {
                    string KodeBahan = Data.GetValue(0).ToString();
                    string namaBahan = Data.GetValue(1).ToString();
                    string satuanBahan = Data.GetValue(2).ToString();
                    int stockBahan = int.Parse(Data.GetValue(3).ToString());

                    string kodeSupplier = Data.GetValue(4).ToString();
                    string namaSupplier = Data.GetValue(5).ToString();
                    string alamatSupplier = Data.GetValue(6).ToString();

                    int HargaSatuan = int.Parse(Data.GetValue(7).ToString());

                    BahanBaku BB = new BahanBaku(KodeBahan, namaBahan, satuanBahan, stockBahan);
                    Supplier SS = new Supplier(kodeSupplier, namaSupplier, alamatSupplier);
                    DetilBahanBaku DBB = new DetilBahanBaku(BB, SS, HargaSatuan);
                    listDetilBahanBaku.Add(DBB);
                }
                MSC.Dispose();
                Data.Dispose();
                return "sukses";
            }
            catch(Exception E)
            {
                return E.Message;
            }
        }

        
    }
}

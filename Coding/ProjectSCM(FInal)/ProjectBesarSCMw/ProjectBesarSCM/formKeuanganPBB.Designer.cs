﻿namespace ProjectBesarSCM
{
    partial class formKeuanganPBB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelKodePembelian = new System.Windows.Forms.Label();
            this.labelInfoKodePembelian = new System.Windows.Forms.Label();
            this.labelInfoBahanBaku = new System.Windows.Forms.Label();
            this.comboBoxBahanBaku = new System.Windows.Forms.ComboBox();
            this.comboBoxSupplier = new System.Windows.Forms.ComboBox();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.labelJumlah = new System.Windows.Forms.Label();
            this.labelHarga = new System.Windows.Forms.Label();
            this.labelHargaSatuan = new System.Windows.Forms.Label();
            this.labelInfoTotalHarga = new System.Windows.Forms.Label();
            this.labelTotalHarga = new System.Windows.Forms.Label();
            this.dataGridViewInfo = new System.Windows.Forms.DataGridView();
            this.namaBahan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HargaSatuan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Jumlah = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TanggalDatang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelInfoTotal = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelRp = new System.Windows.Forms.Label();
            this.labelRp2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelHello = new System.Windows.Forms.Label();
            this.buttonSupplier = new System.Windows.Forms.Button();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.buttonKeluar = new System.Windows.Forms.Button();
            this.buttonHapus = new System.Windows.Forms.Button();
            this.buttonBeli = new System.Windows.Forms.Button();
            this.numericUpDownJumlah = new System.Windows.Forms.NumericUpDown();
            this.buttonS = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJumlah)).BeginInit();
            this.SuspendLayout();
            // 
            // labelKodePembelian
            // 
            this.labelKodePembelian.AutoSize = true;
            this.labelKodePembelian.Location = new System.Drawing.Point(61, 85);
            this.labelKodePembelian.Name = "labelKodePembelian";
            this.labelKodePembelian.Size = new System.Drawing.Size(98, 13);
            this.labelKodePembelian.TabIndex = 0;
            this.labelKodePembelian.Text = "KODE Pembelian : ";
            // 
            // labelInfoKodePembelian
            // 
            this.labelInfoKodePembelian.AutoSize = true;
            this.labelInfoKodePembelian.Location = new System.Drawing.Point(165, 85);
            this.labelInfoKodePembelian.Name = "labelInfoKodePembelian";
            this.labelInfoKodePembelian.Size = new System.Drawing.Size(48, 13);
            this.labelInfoKodePembelian.TabIndex = 1;
            this.labelInfoKodePembelian.Text = "Ini KOde";
            // 
            // labelInfoBahanBaku
            // 
            this.labelInfoBahanBaku.AutoSize = true;
            this.labelInfoBahanBaku.Location = new System.Drawing.Point(61, 109);
            this.labelInfoBahanBaku.Name = "labelInfoBahanBaku";
            this.labelInfoBahanBaku.Size = new System.Drawing.Size(140, 13);
            this.labelInfoBahanBaku.TabIndex = 2;
            this.labelInfoBahanBaku.Text = "Kode - Nama Bahan Baku : ";
            // 
            // comboBoxBahanBaku
            // 
            this.comboBoxBahanBaku.FormattingEnabled = true;
            this.comboBoxBahanBaku.Location = new System.Drawing.Point(207, 106);
            this.comboBoxBahanBaku.Name = "comboBoxBahanBaku";
            this.comboBoxBahanBaku.Size = new System.Drawing.Size(200, 21);
            this.comboBoxBahanBaku.TabIndex = 3;
            this.comboBoxBahanBaku.SelectedIndexChanged += new System.EventHandler(this.comboBoxBahanBaku_SelectedIndexChanged);
            // 
            // comboBoxSupplier
            // 
            this.comboBoxSupplier.FormattingEnabled = true;
            this.comboBoxSupplier.Location = new System.Drawing.Point(168, 136);
            this.comboBoxSupplier.Name = "comboBoxSupplier";
            this.comboBoxSupplier.Size = new System.Drawing.Size(196, 21);
            this.comboBoxSupplier.TabIndex = 5;
            this.comboBoxSupplier.SelectedIndexChanged += new System.EventHandler(this.comboBoxSupplier_SelectedIndexChanged);
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.Location = new System.Drawing.Point(111, 139);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(54, 13);
            this.labelSupplier.TabIndex = 4;
            this.labelSupplier.Text = "Supplier : ";
            // 
            // labelJumlah
            // 
            this.labelJumlah.AutoSize = true;
            this.labelJumlah.Location = new System.Drawing.Point(90, 166);
            this.labelJumlah.Name = "labelJumlah";
            this.labelJumlah.Size = new System.Drawing.Size(69, 13);
            this.labelJumlah.TabIndex = 6;
            this.labelJumlah.Text = "Jumlah Beli : ";
            // 
            // labelHarga
            // 
            this.labelHarga.AutoSize = true;
            this.labelHarga.Location = new System.Drawing.Point(503, 139);
            this.labelHarga.Name = "labelHarga";
            this.labelHarga.Size = new System.Drawing.Size(13, 13);
            this.labelHarga.TabIndex = 9;
            this.labelHarga.Text = "0";
            // 
            // labelHargaSatuan
            // 
            this.labelHargaSatuan.AutoSize = true;
            this.labelHargaSatuan.Location = new System.Drawing.Point(385, 139);
            this.labelHargaSatuan.Name = "labelHargaSatuan";
            this.labelHargaSatuan.Size = new System.Drawing.Size(82, 13);
            this.labelHargaSatuan.TabIndex = 8;
            this.labelHargaSatuan.Text = "Harga Satuan : ";
            // 
            // labelInfoTotalHarga
            // 
            this.labelInfoTotalHarga.AutoSize = true;
            this.labelInfoTotalHarga.Location = new System.Drawing.Point(195, 190);
            this.labelInfoTotalHarga.Name = "labelInfoTotalHarga";
            this.labelInfoTotalHarga.Size = new System.Drawing.Size(13, 13);
            this.labelInfoTotalHarga.TabIndex = 11;
            this.labelInfoTotalHarga.Text = "0";
            // 
            // labelTotalHarga
            // 
            this.labelTotalHarga.AutoSize = true;
            this.labelTotalHarga.Location = new System.Drawing.Point(90, 190);
            this.labelTotalHarga.Name = "labelTotalHarga";
            this.labelTotalHarga.Size = new System.Drawing.Size(72, 13);
            this.labelTotalHarga.TabIndex = 10;
            this.labelTotalHarga.Text = "Total Harga : ";
            // 
            // dataGridViewInfo
            // 
            this.dataGridViewInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.namaBahan,
            this.Supplier,
            this.HargaSatuan,
            this.Jumlah,
            this.Total,
            this.Status,
            this.TanggalDatang});
            this.dataGridViewInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewInfo.Location = new System.Drawing.Point(47, 256);
            this.dataGridViewInfo.Name = "dataGridViewInfo";
            this.dataGridViewInfo.Size = new System.Drawing.Size(563, 89);
            this.dataGridViewInfo.TabIndex = 12;
            // 
            // namaBahan
            // 
            this.namaBahan.HeaderText = "Nama Bahan";
            this.namaBahan.Name = "namaBahan";
            this.namaBahan.Width = 75;
            // 
            // Supplier
            // 
            this.Supplier.HeaderText = "Supplier";
            this.Supplier.Name = "Supplier";
            this.Supplier.Width = 75;
            // 
            // HargaSatuan
            // 
            this.HargaSatuan.HeaderText = "Harga Satuan";
            this.HargaSatuan.Name = "HargaSatuan";
            this.HargaSatuan.Width = 75;
            // 
            // Jumlah
            // 
            this.Jumlah.HeaderText = "Jumlah";
            this.Jumlah.Name = "Jumlah";
            this.Jumlah.Width = 75;
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.Width = 75;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Width = 75;
            // 
            // TanggalDatang
            // 
            this.TanggalDatang.HeaderText = "Tanggal Datang";
            this.TanggalDatang.Name = "TanggalDatang";
            this.TanggalDatang.Width = 75;
            // 
            // labelInfoTotal
            // 
            this.labelInfoTotal.ForeColor = System.Drawing.Color.Red;
            this.labelInfoTotal.Location = new System.Drawing.Point(524, 358);
            this.labelInfoTotal.Name = "labelInfoTotal";
            this.labelInfoTotal.Size = new System.Drawing.Size(79, 13);
            this.labelInfoTotal.TabIndex = 14;
            this.labelInfoTotal.Text = "0";
            // 
            // labelTotal
            // 
            this.labelTotal.ForeColor = System.Drawing.Color.Red;
            this.labelTotal.Location = new System.Drawing.Point(453, 358);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(35, 13);
            this.labelTotal.TabIndex = 13;
            this.labelTotal.Text = "Total:";
            // 
            // labelRp
            // 
            this.labelRp.AutoSize = true;
            this.labelRp.Location = new System.Drawing.Point(473, 139);
            this.labelRp.Name = "labelRp";
            this.labelRp.Size = new System.Drawing.Size(24, 13);
            this.labelRp.TabIndex = 15;
            this.labelRp.Text = "Rp.";
            // 
            // labelRp2
            // 
            this.labelRp2.AutoSize = true;
            this.labelRp2.Location = new System.Drawing.Point(165, 190);
            this.labelRp2.Name = "labelRp2";
            this.labelRp2.Size = new System.Drawing.Size(24, 13);
            this.labelRp2.TabIndex = 16;
            this.labelRp2.Text = "Rp.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(494, 358);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Rp.";
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(466, 13);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(43, 13);
            this.labelHello.TabIndex = 20;
            this.labelHello.Text = "Hello  , ";
            // 
            // buttonSupplier
            // 
            this.buttonSupplier.BackColor = System.Drawing.Color.Transparent;
            this.buttonSupplier.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSupplier.FlatAppearance.BorderSize = 0;
            this.buttonSupplier.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonSupplier.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonSupplier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSupplier.ForeColor = System.Drawing.Color.Transparent;
            this.buttonSupplier.Location = new System.Drawing.Point(90, 386);
            this.buttonSupplier.Name = "buttonSupplier";
            this.buttonSupplier.Size = new System.Drawing.Size(101, 38);
            this.buttonSupplier.TabIndex = 21;
            this.buttonSupplier.UseVisualStyleBackColor = false;
            this.buttonSupplier.Click += new System.EventHandler(this.buttonSupplier_Click);
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.BackColor = System.Drawing.Color.Transparent;
            this.buttonSubmit.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonSubmit.FlatAppearance.BorderSize = 0;
            this.buttonSubmit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonSubmit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSubmit.Location = new System.Drawing.Point(417, 215);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(86, 27);
            this.buttonSubmit.TabIndex = 22;
            this.buttonSubmit.UseVisualStyleBackColor = false;
            // 
            // buttonKeluar
            // 
            this.buttonKeluar.BackColor = System.Drawing.Color.Transparent;
            this.buttonKeluar.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonKeluar.FlatAppearance.BorderSize = 0;
            this.buttonKeluar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonKeluar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonKeluar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeluar.Location = new System.Drawing.Point(559, 392);
            this.buttonKeluar.Name = "buttonKeluar";
            this.buttonKeluar.Size = new System.Drawing.Size(87, 27);
            this.buttonKeluar.TabIndex = 23;
            this.buttonKeluar.UseVisualStyleBackColor = false;
            // 
            // buttonHapus
            // 
            this.buttonHapus.BackColor = System.Drawing.Color.Transparent;
            this.buttonHapus.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonHapus.FlatAppearance.BorderSize = 0;
            this.buttonHapus.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonHapus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonHapus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHapus.Location = new System.Drawing.Point(517, 215);
            this.buttonHapus.Name = "buttonHapus";
            this.buttonHapus.Size = new System.Drawing.Size(86, 27);
            this.buttonHapus.TabIndex = 24;
            this.buttonHapus.UseVisualStyleBackColor = false;
            // 
            // buttonBeli
            // 
            this.buttonBeli.BackColor = System.Drawing.Color.Transparent;
            this.buttonBeli.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonBeli.FlatAppearance.BorderSize = 0;
            this.buttonBeli.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonBeli.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonBeli.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBeli.Location = new System.Drawing.Point(313, 392);
            this.buttonBeli.Name = "buttonBeli";
            this.buttonBeli.Size = new System.Drawing.Size(175, 27);
            this.buttonBeli.TabIndex = 25;
            this.buttonBeli.UseVisualStyleBackColor = false;
            this.buttonBeli.Click += new System.EventHandler(this.buttonBeli_Click);
            // 
            // numericUpDownJumlah
            // 
            this.numericUpDownJumlah.Location = new System.Drawing.Point(169, 164);
            this.numericUpDownJumlah.Name = "numericUpDownJumlah";
            this.numericUpDownJumlah.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownJumlah.TabIndex = 26;
            this.numericUpDownJumlah.ValueChanged += new System.EventHandler(this.numericUpDownJumlah_ValueChanged);
            // 
            // buttonS
            // 
            this.buttonS.BackColor = System.Drawing.Color.Transparent;
            this.buttonS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonS.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonS.FlatAppearance.BorderSize = 0;
            this.buttonS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonS.Location = new System.Drawing.Point(417, 215);
            this.buttonS.Name = "buttonS";
            this.buttonS.Size = new System.Drawing.Size(86, 27);
            this.buttonS.TabIndex = 27;
            this.buttonS.UseVisualStyleBackColor = false;
            this.buttonS.Click += new System.EventHandler(this.buttonS_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.Transparent;
            this.buttonBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonBack.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonBack.FlatAppearance.BorderSize = 0;
            this.buttonBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.Location = new System.Drawing.Point(560, 392);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(86, 27);
            this.buttonBack.TabIndex = 28;
            this.buttonBack.UseVisualStyleBackColor = false;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // formKeuanganPBB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_PBB;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(667, 433);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonS);
            this.Controls.Add(this.numericUpDownJumlah);
            this.Controls.Add(this.buttonBeli);
            this.Controls.Add(this.buttonHapus);
            this.Controls.Add(this.buttonKeluar);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.buttonSupplier);
            this.Controls.Add(this.labelHello);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelRp2);
            this.Controls.Add(this.labelRp);
            this.Controls.Add(this.labelInfoTotal);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.dataGridViewInfo);
            this.Controls.Add(this.labelInfoTotalHarga);
            this.Controls.Add(this.labelTotalHarga);
            this.Controls.Add(this.labelHarga);
            this.Controls.Add(this.labelHargaSatuan);
            this.Controls.Add(this.labelJumlah);
            this.Controls.Add(this.comboBoxSupplier);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.comboBoxBahanBaku);
            this.Controls.Add(this.labelInfoBahanBaku);
            this.Controls.Add(this.labelInfoKodePembelian);
            this.Controls.Add(this.labelKodePembelian);
            this.Name = "formKeuanganPBB";
            this.Text = "formKeuanganPBB";
            this.Load += new System.EventHandler(this.formKeuanganPBB_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJumlah)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelKodePembelian;
        private System.Windows.Forms.Label labelInfoKodePembelian;
        private System.Windows.Forms.Label labelInfoBahanBaku;
        private System.Windows.Forms.ComboBox comboBoxBahanBaku;
        private System.Windows.Forms.ComboBox comboBoxSupplier;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.Label labelJumlah;
        private System.Windows.Forms.Label labelHarga;
        private System.Windows.Forms.Label labelHargaSatuan;
        private System.Windows.Forms.Label labelInfoTotalHarga;
        private System.Windows.Forms.Label labelTotalHarga;
        private System.Windows.Forms.DataGridView dataGridViewInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaBahan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn HargaSatuan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Jumlah;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn TanggalDatang;
        private System.Windows.Forms.Label labelInfoTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelRp;
        private System.Windows.Forms.Label labelRp2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSupplier;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Button buttonKeluar;
        private System.Windows.Forms.Button buttonHapus;
        private System.Windows.Forms.Button buttonBeli;
        private System.Windows.Forms.NumericUpDown numericUpDownJumlah;
        private System.Windows.Forms.Button buttonS;
        private System.Windows.Forms.Button buttonBack;
        public System.Windows.Forms.Label labelHello;
    }
}
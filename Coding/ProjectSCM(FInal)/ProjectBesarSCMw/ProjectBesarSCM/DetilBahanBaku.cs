﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectBesarSCM
{
    public class DetilBahanBaku
    {
        BahanBaku b;
        Supplier s;
        int hargaSatuan;
        
        public BahanBaku B
        {
            get { return b; }
            set { b = value; }
        }    
        public Supplier S
        {
            get { return s; }
            set { s = value; }
        }
        public int HargaSatuan
        {
            get { return hargaSatuan; }
            set { hargaSatuan = value; }
        }
        public DetilBahanBaku()
        {
            b = new BahanBaku();
            s = new Supplier();
            hargaSatuan = 0;

        }
        public DetilBahanBaku(BahanBaku BB, Supplier SS, int harga)
        {
            b = BB;
            s = SS;
            hargaSatuan = harga;
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gudang
{
    public partial class FormStock : Form
    {
        public FormStock()
        {
            InitializeComponent();
        }

        private void buttonCari_Click(object sender, EventArgs e)
        {
            DaftarStock ds = new DaftarStock();
            string nama = textBoxNama.Text;
            string kode = textBoxKode.Text;
            string hasil = ds.CariData(nama, kode);
            if (hasil == "sukses")
            {
                dataGridViewStk.Rows.Clear();
                for(int i = 0; i<ds.JumlahStok; i++)
                {
                    string kd = ds.ListStok[0].KodeBahanBaku;
                    string nm = ds.ListStok[0].NamaBahanBaku;
                    int jmlh = ds.ListStok[0].Jumlah;
                    string nmSp = ds.ListStok[0].NamaSupplier;
                    int hrg = ds.ListStok[0].HargaSatuan;
                    dataGridViewStk.Rows.Add(kd, nm, jmlh, nmSp, hrg);
                }
            }
            else
            {
                MessageBox.Show("Data Tidak Ada");
            }
            

        }

        private void FormStock_Load(object sender, EventArgs e)
        {
            DaftarUser listUser = new DaftarUser();

            string hasilBaca = listUser.CariData("username", "deni7");
            if (hasilBaca == "sukses")
            {
                labelUser.Text = "Hallo, " + listUser.ListPegawai[0].Nama + " - " + listUser.ListPegawai[0].JabatanUser.NamaJabatan;
            }
            else
            {
                MessageBox.Show(hasilBaca);
            }
            DaftarStock ds = new DaftarStock();
            string hasil = ds.BacaSemuaData();
            if(hasil == "sukses")
            {
                for (int i = 0; i < ds.JumlahStok; i++)
                {
                    string kd = ds.ListStok[i].KodeBahanBaku;
                    string nm = ds.ListStok[i].NamaBahanBaku;
                    int jmlh = ds.ListStok[i].Jumlah;
                    string nmSp = ds.ListStok[i].NamaSupplier;
                    int hrg = ds.ListStok[i].HargaSatuan;
                    dataGridViewStk.Rows.Add(kd, nm, jmlh, nmSp, hrg);
                }
            }
            else
            {
                MessageBox.Show(hasil);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

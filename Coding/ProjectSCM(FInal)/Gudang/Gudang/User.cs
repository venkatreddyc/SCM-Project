﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gudang
{
    public class User
    {
        private string idUser;
        private string nama;
        private string alamat;
        private string telepon;
        private string username;
        private string password;
        private Jabatan jabatanUser;

        public string IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }

        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }

        public string Alamat
        {
            get { return alamat; }
            set { alamat = value; }
        }
        public string Telepon
        {
            get { return telepon; }
            set { telepon = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        public Jabatan JabatanUser
        {
            get { return jabatanUser; }
            set { jabatanUser = value; }
        }
        public User()
        {
            idUser = "";
            nama = "";
            alamat = "";
            username = "";
            password = "";
            telepon = "";
            jabatanUser = new Jabatan();
        }
        public User(string kode, string name, string addres, string tel, string user, string pass, Jabatan J)
        {
            telepon = tel;
            idUser = kode;
            nama = name;
            alamat = addres;
            username = user;
            password = pass;
            jabatanUser = J;
        }
    }
}

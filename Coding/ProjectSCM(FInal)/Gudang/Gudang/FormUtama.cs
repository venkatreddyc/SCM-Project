﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;
using Gudang;

namespace Gudang
{
    public partial class FormUtama : Form
    {
        public FormUtama()
        {
            InitializeComponent();
        }

        public int status = 0;

        private void FormUtama_Load(object sender, EventArgs e)
        {
            listBox1.Items.Add("PEMBERITAHUAN");
            spk k  = new spk();
            string hasil = k.bacaSemuaData();
            if(hasil == "sukses")
            {
                for (int i = 0; i < k.JumlahID; i++)
                {
                    listBox1.Items.Add(k.ListSPK[i].Tgl.ToShortDateString() +" - Pesan baru dengan kode " +  k.ListSPK[i].Id);
                 }
            }
            
            if(status == 0)
            {
                
            }
            else
            {
                FormHome FH = (FormHome)this.Owner;
                int stat = FH.status;

                if (stat == 1)
                {
                    button1.Enabled = false;
                    button4.Enabled = false;
                    button2.Enabled = false;
                    status = 1;

                }
                labelJabat.Text = FH.label3.Text;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormDaftarBarang fdb = new FormDaftarBarang();
            fdb.Owner = this;
            fdb.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormKonfrimasi FK = new FormKonfrimasi();
            FK.Owner = this;
            FK.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Laporan laporan = new Laporan();
            laporan.Owner = this;
            laporan.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FormStock fs = new FormStock();
            fs.Owner = this;
            fs.Show();
        }
    }
}

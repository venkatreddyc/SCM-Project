﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;
using Gudang;
using ProjectBesarSCM;
using Admin;

namespace WindowsFormsApplication1
{
    public partial class FormHome : Form
    {
        public int status = 0;
        public int Status2 = 0;
        public FormHome()
        {
            InitializeComponent();
        }
        public string Nama = "";

        private void button1_MouseHover(object sender, EventArgs e)
        {
            buttonVerifikasi.BackColor = Color.WhiteSmoke;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            buttonVerifikasi.BackColor = Color.Transparent;
        }

        private void buttonVerifikasi_Click(object sender, EventArgs e)
        {
            FormVerifikasi FV = new FormVerifikasi();
            FV.Owner = this;
            FV.Show();

        }

        private void buttonGudang_Click(object sender, EventArgs e)
        {
            
            FormUtama FU = new FormUtama();
            FU.status = 1;
            FU.Owner = this;
            FU.ShowDialog();

          //  FU.Show();
        }

        private void FormHome_Load_1(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToString("HH:MM");
            label2.Text = DateTime.Now.ToString("dd/MM/yyyy");
            Nama = label3.Text;
            MessageBox.Show(Nama);
            this.IsMdiContainer = true;
            this.Enabled = true;
           
            //Login frmLogin = new Login();
            //frmLogin.Owner = this;
            //frmLogin.ShowDialog();
        }








        private void buttonKeuangan_Click(object sender, EventArgs e)
        {
            FormKeuanganHome fKH = new FormKeuanganHome();
            fKH.Status = 1;
            fKH.Owner = this;
            fKH.Show();
        }

        private void buttonAdmin_Click(object sender, EventArgs e)
        {
            Admin_FormHome aFH = new Admin_FormHome();
            if(status == 1)
            {
                aFH.status = 1;
            }
       
                aFH.Owner = this;
                aFH.Show();
            
           
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class spk
    {
        private string noSpk;
        private int status;
        private string namaPembeli;
        private string alamatPembeli;
        private string namaUser;
        private string alamatUser;
        private string telepon;
        private string pekerjaan;
        private int biaya;
        private int lama;


        public string NoSpk
        {
            get { return noSpk; }
            set { noSpk = value; }
        }
        
        public int Status
        {
            get { return status; }
            set { status = value; }
        }
        
        public string NamaPembeli
        {
            get { return namaPembeli; }
            set { namaPembeli = value; }
        }
        
        public string AlamatPembeli
        {
            get { return alamatPembeli; }
            set { alamatPembeli = value; }
        }
        
        public string NamaUser
        {
            get { return namaUser; }
            set { namaUser = value; }
        }
        
        public string AlamatUser
        {
            get { return alamatUser; }
            set { alamatUser = value; }
        }
        
        public string Telepon
        {
            get { return telepon; }
            set { telepon = value; }
        }
        
        public string Pekerjaan
        {
            get { return pekerjaan; }
            set { pekerjaan = value; }
        }
        
        public int Biaya
        {
            get { return biaya; }
            set { biaya = value; }
        }
       

        public int Lama
        {
            get { return lama; }
            set { lama = value; }
        }


        public spk()
        {
         noSpk = "";
         status= 0;
         namaPembeli= "";
         alamatPembeli="";
         namaUser="";
         alamatUser="";
         telepon="";
         pekerjaan="";
         biaya=0;
         lama=0;

        }

        public spk(string no, int stat, string np, string ap, string nu, string au, string telp, string pkrj, int b, int l)
        {
            noSpk = no;
            status = stat;
            namaPembeli = np;
            alamatPembeli = ap;
            namaUser = nu;
            alamatUser = au;
            telepon = telp;
            pekerjaan = pkrj;
            biaya = b;
            lama = l;
        }

    }
}

﻿namespace WindowsFormsApplication1
{
    partial class FormVerifikasi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVerifikasi));
            this.buttonKembali = new System.Windows.Forms.Button();
            this.buttonTambahKomentar = new System.Windows.Forms.Button();
            this.buttonVerifikasi = new System.Windows.Forms.Button();
            this.listBoxInfo = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxSpk = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonKembali
            // 
            this.buttonKembali.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonKembali.Location = new System.Drawing.Point(722, 390);
            this.buttonKembali.Name = "buttonKembali";
            this.buttonKembali.Size = new System.Drawing.Size(121, 43);
            this.buttonKembali.TabIndex = 9;
            this.buttonKembali.Text = "KEMBALI";
            this.buttonKembali.UseVisualStyleBackColor = true;
            this.buttonKembali.Click += new System.EventHandler(this.buttonKembali_Click);
            // 
            // buttonTambahKomentar
            // 
            this.buttonTambahKomentar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTambahKomentar.Location = new System.Drawing.Point(92, 390);
            this.buttonTambahKomentar.Name = "buttonTambahKomentar";
            this.buttonTambahKomentar.Size = new System.Drawing.Size(219, 43);
            this.buttonTambahKomentar.TabIndex = 8;
            this.buttonTambahKomentar.Text = "TAMBAH KOMENTAR";
            this.buttonTambahKomentar.UseVisualStyleBackColor = true;
            this.buttonTambahKomentar.Click += new System.EventHandler(this.buttonTambahKomentar_Click);
            // 
            // buttonVerifikasi
            // 
            this.buttonVerifikasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVerifikasi.Location = new System.Drawing.Point(317, 390);
            this.buttonVerifikasi.Name = "buttonVerifikasi";
            this.buttonVerifikasi.Size = new System.Drawing.Size(219, 43);
            this.buttonVerifikasi.TabIndex = 12;
            this.buttonVerifikasi.Text = "VERIFIKASI SPK";
            this.buttonVerifikasi.UseVisualStyleBackColor = true;
            // 
            // listBoxInfo
            // 
            this.listBoxInfo.FormattingEnabled = true;
            this.listBoxInfo.Location = new System.Drawing.Point(78, 140);
            this.listBoxInfo.Name = "listBoxInfo";
            this.listBoxInfo.Size = new System.Drawing.Size(666, 238);
            this.listBoxInfo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(153, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "CARI NO SPK :";
            // 
            // comboBoxSpk
            // 
            this.comboBoxSpk.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSpk.FormattingEnabled = true;
            this.comboBoxSpk.ItemHeight = 24;
            this.comboBoxSpk.Location = new System.Drawing.Point(293, 91);
            this.comboBoxSpk.Name = "comboBoxSpk";
            this.comboBoxSpk.Size = new System.Drawing.Size(315, 32);
            this.comboBoxSpk.TabIndex = 15;
            this.comboBoxSpk.SelectedIndexChanged += new System.EventHandler(this.comboBoxSpk_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(602, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "label3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 415);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "label5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 399);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "label4";
            // 
            // FormVerifikasi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(854, 435);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxSpk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxInfo);
            this.Controls.Add(this.buttonVerifikasi);
            this.Controls.Add(this.buttonKembali);
            this.Controls.Add(this.buttonTambahKomentar);
            this.Name = "FormVerifikasi";
            this.Text = " ";
            this.Load += new System.EventHandler(this.FormVerifikasi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonKembali;
        private System.Windows.Forms.Button buttonTambahKomentar;
        private System.Windows.Forms.Button buttonVerifikasi;
        private System.Windows.Forms.ListBox listBoxInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxSpk;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
    }
}
﻿namespace Admin
{
    partial class Admin_FormJadwalProduksi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelDateTime = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePickerBuat = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerMulai = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerSelesai = new System.Windows.Forms.DateTimePicker();
            this.textBoxJangkaWaktu = new System.Windows.Forms.TextBox();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.comboBoxNo = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.btnTambahMesin = new System.Windows.Forms.Button();
            this.timer1_A_FJP = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // labelDateTime
            // 
            this.labelDateTime.AutoSize = true;
            this.labelDateTime.Location = new System.Drawing.Point(13, 13);
            this.labelDateTime.Name = "labelDateTime";
            this.labelDateTime.Size = new System.Drawing.Size(58, 13);
            this.labelDateTime.TabIndex = 0;
            this.labelDateTime.Text = "Date/Time";
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(608, 13);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(29, 13);
            this.labelUser.TabIndex = 1;
            this.labelUser.Text = "User";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "JADWAL PRODUKSI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(81, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "No SPK - Kode Produk : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Tanggal Pembuatan SPK : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(120, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tgl Mulai SPK :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(114, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Tgl Selesai SPK : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(120, 222);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Jangka Waktu : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(126, 257);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Tenaga Kerja : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(473, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Mesin : ";
            // 
            // dateTimePickerBuat
            // 
            this.dateTimePickerBuat.Location = new System.Drawing.Point(212, 117);
            this.dateTimePickerBuat.Name = "dateTimePickerBuat";
            this.dateTimePickerBuat.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerBuat.TabIndex = 12;
            // 
            // dateTimePickerMulai
            // 
            this.dateTimePickerMulai.Location = new System.Drawing.Point(212, 153);
            this.dateTimePickerMulai.Name = "dateTimePickerMulai";
            this.dateTimePickerMulai.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerMulai.TabIndex = 13;
            // 
            // dateTimePickerSelesai
            // 
            this.dateTimePickerSelesai.Location = new System.Drawing.Point(212, 180);
            this.dateTimePickerSelesai.Name = "dateTimePickerSelesai";
            this.dateTimePickerSelesai.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerSelesai.TabIndex = 14;
            this.dateTimePickerSelesai.ValueChanged += new System.EventHandler(this.dateTimePickerSelesai_ValueChanged);
            // 
            // textBoxJangkaWaktu
            // 
            this.textBoxJangkaWaktu.Enabled = false;
            this.textBoxJangkaWaktu.Location = new System.Drawing.Point(212, 219);
            this.textBoxJangkaWaktu.Name = "textBoxJangkaWaktu";
            this.textBoxJangkaWaktu.Size = new System.Drawing.Size(64, 20);
            this.textBoxJangkaWaktu.TabIndex = 15;
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(484, 463);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(196, 41);
            this.buttonSubmit.TabIndex = 22;
            this.buttonSubmit.Text = "SUBMIT";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(10, 463);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(196, 41);
            this.buttonCancel.TabIndex = 23;
            this.buttonCancel.Text = "CANCEL";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // comboBoxNo
            // 
            this.comboBoxNo.FormattingEnabled = true;
            this.comboBoxNo.Location = new System.Drawing.Point(212, 90);
            this.comboBoxNo.Name = "comboBoxNo";
            this.comboBoxNo.Size = new System.Drawing.Size(200, 21);
            this.comboBoxNo.TabIndex = 26;
            this.comboBoxNo.SelectedIndexChanged += new System.EventHandler(this.comboBoxNo_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(212, 257);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(176, 181);
            this.dataGridView1.TabIndex = 34;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(282, 222);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 35;
            this.label9.Text = "HARI";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(476, 106);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(190, 150);
            this.dataGridView2.TabIndex = 36;
            // 
            // btnTambahMesin
            // 
            this.btnTambahMesin.Location = new System.Drawing.Point(476, 274);
            this.btnTambahMesin.Name = "btnTambahMesin";
            this.btnTambahMesin.Size = new System.Drawing.Size(190, 39);
            this.btnTambahMesin.TabIndex = 37;
            this.btnTambahMesin.Text = "Tambah mesin";
            this.btnTambahMesin.UseVisualStyleBackColor = true;
            this.btnTambahMesin.Click += new System.EventHandler(this.btnTambahMesin_Click);
            // 
            // timer1_A_FJP
            // 
            this.timer1_A_FJP.Tick += new System.EventHandler(this.timer1_A_FJP_Tick);
            // 
            // Admin_FormJadwalProduksi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 516);
            this.Controls.Add(this.btnTambahMesin);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.comboBoxNo);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.textBoxJangkaWaktu);
            this.Controls.Add(this.dateTimePickerSelesai);
            this.Controls.Add(this.dateTimePickerMulai);
            this.Controls.Add(this.dateTimePickerBuat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.labelDateTime);
            this.Name = "Admin_FormJadwalProduksi";
            this.Text = "Jadwal Produksi";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Admin_FormJadwalProduksi_FormClosing);
            this.Load += new System.EventHandler(this.FormJadwalProduksi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDateTime;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimePickerBuat;
        private System.Windows.Forms.DateTimePicker dateTimePickerMulai;
        private System.Windows.Forms.DateTimePicker dateTimePickerSelesai;
        private System.Windows.Forms.TextBox textBoxJangkaWaktu;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ComboBox comboBoxNo;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btnTambahMesin;
        private System.Windows.Forms.Timer timer1_A_FJP;
    }
}
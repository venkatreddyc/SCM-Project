﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class FormPesanTambah : Form
    {

        public FormPesanTambah()
        {
            InitializeComponent();
        }

        private void FormPesanTambah_Load(object sender, EventArgs e)
        {
            labelJam.Text = DateTime.Now.ToString("hh:mm");
            labelTgl.Text = DateTime.Now.ToString("dd/MM/yy");
        }

        private void buttonSimpan_Click(object sender, EventArgs e)
        {

            TambahBahan tb = new TambahBahan(textBoxKode.Text, textBoxNama.Text, textBoxSatuan.Text);
            DTambah daftar = new DTambah();
            string hasil = daftar.TambahData(tb);
            if (hasil == "sukses")
            {
                MessageBox.Show("Data bahan baku telah tersimpan", "Info");
            }
            else
            {
                MessageBox.Show("Data kategori gagal tersimpan, Pesan kesalahan : " + hasil, "Kesalahan");
            }

        }

        private void buttonKembali_Click(object sender, EventArgs e)
        {

            FormPesanTambah frm = new FormPesanTambah();
            frm.Owner = this;
            frm.Show();
        }
    }
}

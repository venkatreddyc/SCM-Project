﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Admin
{
    public partial class Admin_FormPenerimaanBarangJadi : Form
    {
        string admin_username;
        string admin_kategori;
        String[] tampung = new string[9];
        public Admin_FormPenerimaanBarangJadi()
        {
            InitializeComponent();
        }

        private void Admin_FormPenerimaanBarangJadi_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            labelUser.Text = admin_username + " - " + admin_kategori;


            String loadString = @"server=localhost;database=scm;userid=root;password=;";
            MySqlConnection connection = new MySqlConnection(loadString);

            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("Select noSPK from formpembayaran", connection);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBoxSPK.Items.Add(rdr["noSPK"]);
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fail");
            }
        }

        private void buttonKembali_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin_FormHome o = new Admin_FormHome();
            o.ShowDialog();
        }

        private void comboBoxSPK_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonCetak_Click(object sender, EventArgs e)
        {
            

            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("form_penerimaan_bahanbaku_" + comboBoxSPK.Text + ".pdf", FileMode.Create));
            doc.Open();
            for (int i = 0; i < tampung.Length; i++)
            {
                Paragraph para = new Paragraph(tampung[i]);
                doc.Add(para);
            }
            MessageBox.Show("Lihat di Debug untuk lihat PDF");
            doc.Close();
            
        }

        private void comboBoxSPK_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            String loadString = @"server=localhost;database=scm;userid=root;password=;";
            MySqlConnection connection = new MySqlConnection(loadString);

            try
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT c.nama as nama,c.alamat as alamat,s.kodeProduk as kode,s.biaya as biaya,fp.pembayaranSatu as pembayaranSatu,fp.pembayaranDua as pembayaranDua,fp.kekurangan as kekurangan,fp.caraPembayaranSatu as caraPembayaran from spk s INNER JOIN customer c on s.kodeCustomer=c.kodeCustomer INNER JOIN formpembayaran fp on s.noSPK=fp.noSPK WHERE s.noSPK='" + comboBoxSPK.Text + "'", connection);
                MySqlDataReader reader1 = cmd.ExecuteReader();
                while (reader1.Read())
                {
                    tampung[0] = "Nama : " + reader1["nama"];
                    tampung[1] = "Alamat : " + reader1["alamat"];
                    tampung[2] = "Kode Produk : " + reader1["kode"];
                    tampung[3] = "Total Harga : " + reader1["biaya"];
                    tampung[4] = "Pembayaran 1 : " + reader1["pembayaranSatu"];
                    if (reader1["pembayaranDua"] == DBNull.Value)
                    {
                        tampung[5] = "Pembayaran 2 : -";
                    }
                    else
                    {
                        tampung[5] = "Pembayaran 2 : " + reader1["pembayaranDua"];
                    }

                    tampung[6] = "Kekurangan : " + reader1["kekurangan"];
                    tampung[7] = "Cara pembayaran : " + reader1["caraPembayaran"];
                    tampung[8] = "Tanggal penerimaan : " + dateTimePickerTanggal.Value;

                    listBoxInfo.Items.Add("Nama : " + reader1["nama"]);
                    listBoxInfo.Items.Add("Alamat : " + reader1["alamat"]);
                    listBoxInfo.Items.Add("Kode Produk : " + reader1["kode"]);
                    listBoxInfo.Items.Add("Total Harga : " + reader1["biaya"]);
                    listBoxInfo.Items.Add("Pembayaran1 : " + reader1["pembayaranSatu"]);
                    if (reader1["pembayaranDua"] == DBNull.Value)
                    {
                        listBoxInfo.Items.Add("Pembayaran2 : -");
                    }
                    else
                    {
                        listBoxInfo.Items.Add("Pembayaran2 : " + reader1["pembayaranDua"]);
                    }
                    listBoxInfo.Items.Add("Kekurangan : " + reader1["kekurangan"]);
                    listBoxInfo.Items.Add("Cara pembayaran : " + reader1["caraPembayaran"]);
                    listBoxInfo.Items.Add("Tanggal Penerimaan : " + dateTimePickerTanggal.Value);
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Belum melakukan pembayaran");
            }
        }

        private void buttonKembali_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class Admin_FormJadwalProduksi : Form
    {
        string dt1;
        string dt2;
        string b;
        string NamaTK,NamaMesin = "";
        string admin_username, admin_kategori = "";

        //public Admin_FormJadwalProduksi()
        //{
        //    InitializeComponent();
            
        //}
        public Admin_FormJadwalProduksi()
        {
            InitializeComponent();
        }

        private void FormJadwalProduksi_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            //this.IsMdiContainer = true;
            // labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            //labelUser.Text = FormConnect.username + " - " + FormConnect.kategori_user;

            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();
                var query = "SELECT nama,noMesin FROM mesin";
                var queryNoSPKdanKodeProduksi = "SELECT noSPK, kodeProduk, pekerjaan, tanggal, tanggalMulai from spk";
                using (var command1 = new MySqlCommand(queryNoSPKdanKodeProduksi, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        //Iterate through the rows and add it to the combobox's items
                        string a = "";
                        while (reader1.Read())
                        {
                            a = reader1.GetString("noSPK") + " - " + reader1.GetString("kodeProduk");
                            b = reader1.GetString("noSPK");
                            comboBoxNo.Items.Add(a);
                            comboBoxNo.SelectedIndex = 0;
                        }
                    }
                }
                //Create New DataGridViewTextBoxColumn
                DataGridViewComboBoxColumn cmbColumn = new DataGridViewComboBoxColumn();

                //Bind DataGridView to Datasource
                cmbColumn.HeaderText = "Pilih Mesin";

                //Add TextBoxColumn dynamically to DataGridView
                dataGridView2.Columns.Add(cmbColumn);

                var queryMesin = "SELECT noMesin, nama from mesin";
                using (var command2 = new MySqlCommand(queryMesin, connection))
                {
                    using (var reader2 = command2.ExecuteReader())
                    {
                        //Iterate through the rows and add it to the combobox's items
                        string a = "";
                        while (reader2.Read())
                        {
                            a = reader2.GetString("noMesin") + " - " + reader2.GetString("nama");
                            cmbColumn.Items.Add(a);
                            //comboBoxNo.SelectedIndex = 0;
                        }
                    }
                }
            }
            //Create New DataGridViewTextBoxColumn
            DataGridViewTextBoxColumn textboxColumn = new DataGridViewTextBoxColumn();
            //Bind DataGridView to Datasource
            textboxColumn.HeaderText = "Nama TK";
            //Add TextBoxColumn dynamically to DataGridView
            dataGridView1.Columns.Add(textboxColumn);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            //comboBoxNo.SelectedIndex = 0;
            //textBoxJangkaWaktu.Text = "";
            this.Close();

        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            NamaTK = "";
            NamaMesin = "";
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //Do your task here
                try
                {
                    string fourthColumn = row.Cells[0].Value.ToString();
                    NamaTK += row.Cells[0].Value.ToString() + ";";       
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Data Berhasil Dimasukkan");
                }
            }

            foreach (DataGridViewRow row1 in dataGridView2.Rows)
            {
                //Do your task here
                try
                {
                    string fourthColumn = row1.Cells[0].Value.ToString();
                    NamaMesin += row1.Cells[0].Value.ToString() + ";";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Data Berhasil Dimasukkan");
                }
            }
            //MessageBox.Show(NamaMesin);
            String data = comboBoxNo.SelectedItem.ToString();
            DateTime dtBuat = this.dateTimePickerBuat.Value;
            string dtMulai = dt1;
            string dtSelesai = dt2;
            string jangkaWaktu = textBoxJangkaWaktu.Text;
            string tenagaKerja = NamaTK;
            string mesin = NamaMesin;
            //MessageBox.Show(data+ " " + dtBuat+ " " + dtMulai+ " " + dtSelesai+ " " + jangkaWaktu+ " " + tenagaKerja+ " " + mesin);

            Admin_FormJadwalSubmit s = new Admin_FormJadwalSubmit(data,dtBuat, dtMulai,dtSelesai,jangkaWaktu,tenagaKerja,NamaMesin);
            s.ShowDialog();
        }

        private void btnTambahMesin_Click(object sender, EventArgs e)
        {
            Admin_FormTambahMesin fts = new Admin_FormTambahMesin();
            //fts.MdiParent = this;
            fts.Owner = this;
            fts.Show();
        }

        private void timer1_A_FJP_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            // this.labelDateTime.Text = dt.ToString("yyyy-MM-dd/hh:mm:ss");
            this.labelDateTime.Text = dt.ToString();
        }

        private void dateTimePickerSelesai_ValueChanged(object sender, EventArgs e)
        {
            
            dt1 = dateTimePickerMulai.Value.ToString("yyyy-MM-dd"); ;
            dt2 = dateTimePickerSelesai.Value.ToString("yyyy-MM-dd"); ;
            textBoxJangkaWaktu.Text = (dateTimePickerSelesai.Value.Subtract(dateTimePickerMulai.Value).Days.ToString());
            //TimeSpan tspan = dt2 - dt1;

            //int differenceInDays = tspan.Days;

            //textBoxJangkaWaktu.Text = days.ToString();
        }

        private void comboBoxNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();

                //berubah
                var queryNoSPKdanKodeProduksi = "SELECT tanggal, tanggalMulai from spk where spk.noSPK=" + b;
                MessageBox.Show(b);
                using (var command1 = new MySqlCommand(queryNoSPKdanKodeProduksi, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        //Iterate through the rows and add it to the combobox's items
                        string a = "";
                        while (reader1.Read())
                        {

                            dateTimePickerBuat.Value = reader1.GetDateTime("tanggal");
                            dateTimePickerMulai.Value = reader1.GetDateTime("tanggalMulai");
                        }
                    }
                }
            }
        
        }

        private void Admin_FormJadwalProduksi_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            Admin_FormHome fh = new Admin_FormHome();
            fh.ShowDialog();
        }
        
    }
}

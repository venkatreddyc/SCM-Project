﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class Admin_FormPenggunaanKembalikan : Form
    {
        string admin_username;
        string admin_kategori;
        string noSPK = "1";

        public Admin_FormPenggunaanKembalikan()
        {
            InitializeComponent();
            //this.admin_username = u;
            //this.admin_kategori = k;
            //this.noSPK = no;
        }
      
        private void Admin_FormPenggunaanKembalikan_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            label2.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            label1.Text = admin_username + " - " + admin_kategori;

            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();
                var query = "select b.nama, f.sisaPenggunaan from formpenggunaandetil f inner join bahanbaku b on f.kodeBahanBaku = b.kodeBahanBaku where f.noSPK='"+noSPK+"'";

                MySqlDataAdapter MyDA = new MySqlDataAdapter();
                string sqlSelectAll = "SELECT * from info";
                MyDA.SelectCommand = new MySqlCommand(query, connection);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;


                dataGridView1.DataSource = bSource;

                //Create New DataGridViewTextBoxColumn
                DataGridViewTextBoxColumn cmbColumn = new DataGridViewTextBoxColumn();
                cmbColumn.Width = 290;

                //Bind DataGridView to Datasource
                cmbColumn.HeaderText = "Keterangan";

                //Add TextBoxColumn dynamically to DataGridView
                dataGridView1.Columns.Add(cmbColumn);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //Do your task here
                try
                {
                    string namaBB = "";
                    
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        namaBB = row.Cells[i].Value.ToString() + ";";
                    }
                    MessageBox.Show(namaBB);
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Data Berhasil Dimasukkan");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}



//select d.jumlah - f.jumlahKeluar as Sisa
//from formpenggunaandetil f inner join formpemesanandetil d on f.noSPK = d.noSPK inner join bahanbaku b on b.kodeBahanBaku = d.kodeBahanBaku
//where d.kodeBahanBaku = f.kodeBahanBaku 
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class Admin_FormJadwalProduksi : Form
    {
        string admin_username;
        string admin_kategori;
        DateTime dt1;
        DateTime dt2;
        string NamaTK,NamaMesin = "";

        public Admin_FormJadwalProduksi()
        {
            InitializeComponent();
            timer1_A_FJP.Start();
        }
        //public FormJadwalProduksi(string u, string k)
        //{
        //    InitializeComponent();
        //    this.admin_username = u;
        //    this.admin_kategori = k;
        //}

        private void FormJadwalProduksi_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            //this.IsMdiContainer = true;
            // labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            labelUser.Text = admin_username + " - " + admin_kategori;

            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();
                var query = "SELECT nama,noMesin FROM mesin";
                //using (var command = new MySqlCommand(query, connection))
                //{
                //    using (var reader = command.ExecuteReader())
                //    {
                //        //Iterate through the rows and add it to the combobox's items
                //        while (reader.Read())
                //        {
                //            string nama = reader.GetString("noMesin")+ "-" + reader.GetString("nama");
                //            comboBox1.Items.Add(nama);
                //            comboBox2.Items.Add(nama);
                //            comboBox3.Items.Add(nama);
                //        }
                //    }
                //}

                var queryNoSPKdanKodeProduksi = "SELECT noSPK, kodeProduk, pekerjaan from spk";
                using (var command1 = new MySqlCommand(queryNoSPKdanKodeProduksi, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        //Iterate through the rows and add it to the combobox's items
                        string a = "";
                        while (reader1.Read())
                        {
                            a = reader1.GetString("noSPK") + " - " + reader1.GetString("kodeProduk");
                            comboBoxNo.Items.Add(a);
                            comboBoxNo.SelectedIndex = 0;
                        }
                    }
                }
                //Create New DataGridViewTextBoxColumn
                DataGridViewComboBoxColumn cmbColumn = new DataGridViewComboBoxColumn();

                //Bind DataGridView to Datasource
                cmbColumn.HeaderText = "Pilih Mesin";

                //Add TextBoxColumn dynamically to DataGridView
                dataGridView2.Columns.Add(cmbColumn);

                var queryMesin = "SELECT noMesin, nama from mesin";
                using (var command2 = new MySqlCommand(queryMesin, connection))
                {
                    using (var reader2 = command2.ExecuteReader())
                    {
                        //Iterate through the rows and add it to the combobox's items
                        string a = "";
                        while (reader2.Read())
                        {
                            a = reader2.GetString("noMesin") + " - " + reader2.GetString("nama");
                            cmbColumn.Items.Add(a);
                            //comboBoxNo.SelectedIndex = 0;
                        }
                    }
                }
            }
            //Create New DataGridViewTextBoxColumn
            DataGridViewTextBoxColumn textboxColumn = new DataGridViewTextBoxColumn();
            //Bind DataGridView to Datasource
            textboxColumn.HeaderText = "Nama TK";
            //Add TextBoxColumn dynamically to DataGridView
            dataGridView1.Columns.Add(textboxColumn);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            //comboBoxNo.SelectedIndex = 0;
            //textBoxJangkaWaktu.Text = "";
            this.Close();
            Admin_FormJadwalProduksi jp=new Admin_FormJadwalProduksi();
            jp.ShowDialog();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //Do your task here
                try
                {
                    string fourthColumn = row.Cells[0].Value.ToString();
                    NamaTK += row.Cells[0].Value.ToString() + ";";       
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Data Berhasil Dimasukkan");
                }
            }

            foreach (DataGridViewRow row1 in dataGridView2.Rows)
            {
                //Do your task here
                try
                {
                    string fourthColumn = row1.Cells[0].Value.ToString();
                    NamaMesin += row1.Cells[0].Value.ToString() + ";";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Data Berhasil Dimasukkan");
                }
            }
            //MessageBox.Show(NamaMesin);
            String data = comboBoxNo.SelectedItem.ToString();
            DateTime dtBuat = this.dateTimePickerBuat.Value;
            DateTime dtMulai = dt1;
            DateTime dtSelesai = dt2;
            string jangkaWaktu = textBoxJangkaWaktu.Text;
            string tenagaKerja = NamaTK;
            string mesin = NamaMesin;
            //MessageBox.Show(data+ " " + dtBuat+ " " + dtMulai+ " " + dtSelesai+ " " + jangkaWaktu+ " " + tenagaKerja+ " " + mesin);

            Admin_FormJadwalSubmit s = new Admin_FormJadwalSubmit(data,dtBuat,dtMulai,dtSelesai,jangkaWaktu,tenagaKerja,NamaMesin);
            s.ShowDialog();
        }

        private void btnTambahMesin_Click(object sender, EventArgs e)
        {
            Admin_FormTambahMesin fts = new Admin_FormTambahMesin();
            //fts.MdiParent = this;
            fts.Owner = this;
            fts.Show();
        }

        private void timer1_A_FJP_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            // this.labelDateTime.Text = dt.ToString("yyyy-MM-dd/hh:mm:ss");
            this.labelDateTime.Text = dt.ToString();
        }

        private void dateTimePickerSelesai_ValueChanged(object sender, EventArgs e)
        {
            
            dt1 = dateTimePickerMulai.Value;
            dt2 = dateTimePickerSelesai.Value;
            textBoxJangkaWaktu.Text = (dt2.Subtract(dt1).Days.ToString());
            //TimeSpan tspan = dt2 - dt1;

            //int differenceInDays = tspan.Days;

            //textBoxJangkaWaktu.Text = days.ToString();
        }
    }
}

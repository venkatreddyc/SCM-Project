﻿namespace Admin
{
    partial class FormStatusSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cboSPK = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lboStatusSPK = new System.Windows.Forms.ListBox();
            this.btnCetak = new System.Windows.Forms.Button();
            this.btnKembali = new System.Windows.Forms.Button();
            this.btnKomentar = new System.Windows.Forms.Button();
            this.lblDateTime_A_FTM = new System.Windows.Forms.Label();
            this.lblUser_A_FTM = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // cboSPK
            // 
            this.cboSPK.FormattingEnabled = true;
            this.cboSPK.Location = new System.Drawing.Point(161, 52);
            this.cboSPK.Margin = new System.Windows.Forms.Padding(2);
            this.cboSPK.Name = "cboSPK";
            this.cboSPK.Size = new System.Drawing.Size(165, 21);
            this.cboSPK.TabIndex = 3;
            this.cboSPK.SelectedIndexChanged += new System.EventHandler(this.cboSPK_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 54);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Cari No SPK : ";
            // 
            // lboStatusSPK
            // 
            this.lboStatusSPK.FormattingEnabled = true;
            this.lboStatusSPK.Location = new System.Drawing.Point(77, 97);
            this.lboStatusSPK.Margin = new System.Windows.Forms.Padding(2);
            this.lboStatusSPK.Name = "lboStatusSPK";
            this.lboStatusSPK.Size = new System.Drawing.Size(249, 225);
            this.lboStatusSPK.TabIndex = 4;
            // 
            // btnCetak
            // 
            this.btnCetak.Location = new System.Drawing.Point(190, 332);
            this.btnCetak.Margin = new System.Windows.Forms.Padding(2);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(56, 19);
            this.btnCetak.TabIndex = 5;
            this.btnCetak.Text = "CETAK";
            this.btnCetak.UseVisualStyleBackColor = true;
            // 
            // btnKembali
            // 
            this.btnKembali.Location = new System.Drawing.Point(250, 332);
            this.btnKembali.Margin = new System.Windows.Forms.Padding(2);
            this.btnKembali.Name = "btnKembali";
            this.btnKembali.Size = new System.Drawing.Size(75, 19);
            this.btnKembali.TabIndex = 6;
            this.btnKembali.Text = "KEMBALI";
            this.btnKembali.UseVisualStyleBackColor = true;
            this.btnKembali.Click += new System.EventHandler(this.btnKembali_Click);
            // 
            // btnKomentar
            // 
            this.btnKomentar.Location = new System.Drawing.Point(78, 332);
            this.btnKomentar.Margin = new System.Windows.Forms.Padding(2);
            this.btnKomentar.Name = "btnKomentar";
            this.btnKomentar.Size = new System.Drawing.Size(89, 19);
            this.btnKomentar.TabIndex = 7;
            this.btnKomentar.Text = "KOMENTAR";
            this.btnKomentar.UseVisualStyleBackColor = true;
            this.btnKomentar.Click += new System.EventHandler(this.btnKomentar_Click);
            // 
            // lblDateTime_A_FTM
            // 
            this.lblDateTime_A_FTM.AutoSize = true;
            this.lblDateTime_A_FTM.Location = new System.Drawing.Point(12, 25);
            this.lblDateTime_A_FTM.Name = "lblDateTime_A_FTM";
            this.lblDateTime_A_FTM.Size = new System.Drawing.Size(58, 13);
            this.lblDateTime_A_FTM.TabIndex = 8;
            this.lblDateTime_A_FTM.Text = "Date/Time";
            // 
            // lblUser_A_FTM
            // 
            this.lblUser_A_FTM.AutoSize = true;
            this.lblUser_A_FTM.Location = new System.Drawing.Point(350, 25);
            this.lblUser_A_FTM.Name = "lblUser_A_FTM";
            this.lblUser_A_FTM.Size = new System.Drawing.Size(29, 13);
            this.lblUser_A_FTM.TabIndex = 9;
            this.lblUser_A_FTM.Text = "User";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormStatusSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 388);
            this.Controls.Add(this.lblUser_A_FTM);
            this.Controls.Add(this.lblDateTime_A_FTM);
            this.Controls.Add(this.btnKomentar);
            this.Controls.Add(this.btnKembali);
            this.Controls.Add(this.btnCetak);
            this.Controls.Add(this.lboStatusSPK);
            this.Controls.Add(this.cboSPK);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormStatusSPK";
            this.Text = "FormStatusSPK";
            this.Load += new System.EventHandler(this.FormStatusSPK_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboSPK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lboStatusSPK;
        private System.Windows.Forms.Button btnCetak;
        private System.Windows.Forms.Button btnKembali;
        private System.Windows.Forms.Button btnKomentar;
        private System.Windows.Forms.Label lblDateTime_A_FTM;
        private System.Windows.Forms.Label lblUser_A_FTM;
        private System.Windows.Forms.Timer timer1;
    }
}
﻿namespace Admin
{
    partial class Admin_FormSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonBuatSPK = new System.Windows.Forms.Button();
            this.btnStatusSPK = new System.Windows.Forms.Button();
            this.btnHapusSPK = new System.Windows.Forms.Button();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // buttonBuatSPK
            // 
            this.buttonBuatSPK.Location = new System.Drawing.Point(33, 102);
            this.buttonBuatSPK.Name = "buttonBuatSPK";
            this.buttonBuatSPK.Size = new System.Drawing.Size(93, 70);
            this.buttonBuatSPK.TabIndex = 3;
            this.buttonBuatSPK.Text = "BUAT SPK";
            this.buttonBuatSPK.UseVisualStyleBackColor = true;
            // 
            // btnStatusSPK
            // 
            this.btnStatusSPK.Location = new System.Drawing.Point(176, 102);
            this.btnStatusSPK.Name = "btnStatusSPK";
            this.btnStatusSPK.Size = new System.Drawing.Size(93, 70);
            this.btnStatusSPK.TabIndex = 4;
            this.btnStatusSPK.Text = "STATUS SPK";
            this.btnStatusSPK.UseVisualStyleBackColor = true;
            this.btnStatusSPK.Click += new System.EventHandler(this.btnStatusSPK_Click);
            // 
            // btnHapusSPK
            // 
            this.btnHapusSPK.Location = new System.Drawing.Point(310, 102);
            this.btnHapusSPK.Name = "btnHapusSPK";
            this.btnHapusSPK.Size = new System.Drawing.Size(93, 70);
            this.btnHapusSPK.TabIndex = 5;
            this.btnHapusSPK.Text = "HAPUS SPK";
            this.btnHapusSPK.UseVisualStyleBackColor = true;
            this.btnHapusSPK.Click += new System.EventHandler(this.btnHapusSPK_Click);
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Location = new System.Drawing.Point(41, 25);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(58, 13);
            this.labelDate.TabIndex = 7;
            this.labelDate.Text = "Date/Time";
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(370, 21);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(29, 13);
            this.labelUser.TabIndex = 6;
            this.labelUser.Text = "User";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Admin_FormSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 288);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.btnHapusSPK);
            this.Controls.Add(this.btnStatusSPK);
            this.Controls.Add(this.buttonBuatSPK);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Admin_FormSPK";
            this.Text = "Admin_FormSPK";
            this.Load += new System.EventHandler(this.Admin_FormSPK_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonBuatSPK;
        private System.Windows.Forms.Button btnStatusSPK;
        private System.Windows.Forms.Button btnHapusSPK;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Timer timer1;
    }
}
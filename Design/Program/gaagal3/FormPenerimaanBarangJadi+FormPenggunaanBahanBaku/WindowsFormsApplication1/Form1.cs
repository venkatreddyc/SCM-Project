﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonPenerimaanBarangJadi_Click(object sender, EventArgs e)
        {
            Admin_FormPenerimaanBarangJadi formPenerimaanBJ = new Admin_FormPenerimaanBarangJadi();
            formPenerimaanBJ.ShowDialog();
            this.Hide();
        }

        private void buttonPenggunaanBahanBaku_Click(object sender, EventArgs e)
        {
            Admin_FormPenggunaanBahanBaku formPenggunaanBK = new Admin_FormPenggunaanBahanBaku();
            formPenggunaanBK.ShowDialog();
            this.Hide();
        }
    }
}

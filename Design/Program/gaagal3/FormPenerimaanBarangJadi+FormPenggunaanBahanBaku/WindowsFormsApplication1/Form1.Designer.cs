﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPenerimaanBarangJadi = new System.Windows.Forms.Button();
            this.buttonPenggunaanBahanBaku = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonPenerimaanBarangJadi
            // 
            this.buttonPenerimaanBarangJadi.Location = new System.Drawing.Point(159, 81);
            this.buttonPenerimaanBarangJadi.Name = "buttonPenerimaanBarangJadi";
            this.buttonPenerimaanBarangJadi.Size = new System.Drawing.Size(110, 65);
            this.buttonPenerimaanBarangJadi.TabIndex = 0;
            this.buttonPenerimaanBarangJadi.Text = "PENERIMAAN BARANG JADI";
            this.buttonPenerimaanBarangJadi.UseVisualStyleBackColor = true;
            this.buttonPenerimaanBarangJadi.Click += new System.EventHandler(this.buttonPenerimaanBarangJadi_Click);
            // 
            // buttonPenggunaanBahanBaku
            // 
            this.buttonPenggunaanBahanBaku.Location = new System.Drawing.Point(275, 81);
            this.buttonPenggunaanBahanBaku.Name = "buttonPenggunaanBahanBaku";
            this.buttonPenggunaanBahanBaku.Size = new System.Drawing.Size(107, 65);
            this.buttonPenggunaanBahanBaku.TabIndex = 1;
            this.buttonPenggunaanBahanBaku.Text = "PENGGUNAAN BAHAN BAKU";
            this.buttonPenggunaanBahanBaku.UseVisualStyleBackColor = true;
            this.buttonPenggunaanBahanBaku.Click += new System.EventHandler(this.buttonPenggunaanBahanBaku_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 335);
            this.Controls.Add(this.buttonPenggunaanBahanBaku);
            this.Controls.Add(this.buttonPenerimaanBarangJadi);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonPenerimaanBarangJadi;
        private System.Windows.Forms.Button buttonPenggunaanBahanBaku;
    }
}


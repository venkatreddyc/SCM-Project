﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class Admin_FormJadwalSubmit : Form
    {

        string data = "";
        DateTime dtBuat;
        string dtMulai,dtSelesai;
        string jangkaWaktu, tenagaKerja, mesin;
        string lastJadwalID;
        int lastID;
        string[] TK;
        string[] Mesin;
        string noSpk;
        Boolean insertFormJadwal = false;
        Boolean dapatKode = false;

        Koneksi k = new Koneksi();

        private void timer1_A_FJS_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            // this.labelDateTime.Text = dt.ToString("yyyy-MM-dd/hh:mm:ss");
            this.lblDateTime_A_FJS.Text = dt.ToString();
        }

        private void cboVerifikasi_A_FJS_CheckedChanged(object sender, EventArgs e)
        {
            if (cboVerifikasi_A_FJS.Checked)
            {
                btnSubmit_A_FJS.Enabled = true;
            }else
            {
                btnSubmit_A_FJS.Enabled = false;
            }
        }

        private void btnSubmit_A_FJS_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("" + dtSelesai);
            //string date = dtSelesai.ToString("yyyy-mm-dd");
            string idbaru = "J" + (lastID + 1);
            string sql = "Insert into formjadwal(kodeJadwal,tanggalSelesai,status,noSPK,tenagaKerjaSatu,tenagaKerjaDua,tenagaKerjaTiga) values ('" +
                        idbaru + "','" + dtSelesai + "',0,'" + noSpk + "','" + tenagaKerja + "','-','-')";

            MySqlCommand cmd = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("sukses");
                insertFormJadwal = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ini yg formjadwal : " + ex.Message);
            }

            if (insertFormJadwal == true)
            {
                sql = "Select kodeJadwal from formjadwal order by kodeJadwal desc limit 1";

                cmd = new MySqlCommand(sql, k.KoneksiDB);

                try
                {
                    var read = cmd.ExecuteReader();
                    while (read.Read())
                    {
                        lastJadwalID = read.GetString("kodejadwal");
                        dapatKode = true;
                    }
                    read.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ini yang kodejadwal : " + ex.Message);
                }

            }
            if (dapatKode == true)
            {
                for (int i = 0; i < Mesin.Length - 1; i++)
                {
                    if (i != Mesin.Length)
                    {
                        //MessageBox.Show(Mesin[i].ToString());
                        sql = "Insert into jadwalmesin (kodeJadwal,noSPK,noMesin) values('" + lastJadwalID + "','" + noSpk + "','" + Mesin[i].ToString() + "')";
                        cmd = new MySqlCommand(sql, k.KoneksiDB);

                        try
                        {
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("Sukses");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Ini yang jadwalmesin" + ex.Message);
                        }
                    }
                }
            }
        }

        private void btnKembali_A_FJS_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            this.Close();
        }

        public Admin_FormJadwalSubmit(string data, DateTime dtBuat,string dtMulai,string dtSelesai,string jangkaWaktu, string tenagaKerja, string mesin)
        {
            InitializeComponent();
            timer1_A_FJS.Start();
            this.data = data;
            this.dtBuat = dtBuat;
            this.dtMulai = dtMulai;
            this.dtSelesai = dtSelesai;
            this.jangkaWaktu = jangkaWaktu;
            this.tenagaKerja = tenagaKerja;
            this.mesin = mesin;
        }

        private void FormJadwalSubmit_Load(object sender, EventArgs e)
        {
            lblUser_A_FJS.Text = FormConnect.username + " - " + FormConnect.kategori_user;
            k.Connect();
            //MessageBox.Show(data + " " + dtBuat + " " + dtMulai + " " + dtSelesai + " " + jangkaWaktu + " " + tenagaKerja + " " + mesin);
            string [] kodeSpk= data.Split('-');

            listBox1.Items.Add("Tanggal : " +  dtBuat);
            listBox1.Items.Add("SPK / PRAK  : " + kodeSpk[0]);
            noSpk = kodeSpk[0];
            listBox1.Items.Add("Kode Produk : " + kodeSpk[1]);
            listBox1.Items.Add("Tanggal Mulai: " + dtMulai);
            listBox1.Items.Add("Tanggal Selesai: " + dtSelesai);
            listBox1.Items.Add("Jangka waktu : " + jangkaWaktu);
            listBox1.Items.Add("Tenaga kerja : " + tenagaKerja);
            
            TK = tenagaKerja.Split(';');
            
            //MessageBox.Show(TK.Length.ToString());
            //for(int i=0; i < TK.Length-1; i++)
            //{
            //    if(i!=TK.Length)
            //        MessageBox.Show(TK[i].ToString());
            //}
            listBox1.Items.Add("Mesin : " + mesin);
            
            Mesin = mesin.Split(';');
            
            //for (int i = 0; i < Mesin.Length - 1; i++)
            //{
            //    if (i != Mesin.Length)
            //        MessageBox.Show(Mesin[i].ToString());
            //}

            string sql = "Select kodeJadwal from formjadwal order by kodeJadwal desc limit 1";

            MySqlCommand cmd = new MySqlCommand(sql, k.KoneksiDB);

            try
            {
                var read = cmd.ExecuteReader();
                while (read.Read())
                {
                    lastJadwalID = read.GetString("kodejadwal");
                }
                read.Close();

                MessageBox.Show("sukses");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ini waktu load : " + ex.Message);
            }

            //MessageBox.Show(lastJadwalID);
            int panjangID = lastJadwalID.Length;
            //MessageBox.Show(panjangID.ToString());
            string id = lastJadwalID.Substring(1, panjangID-1);
            //MessageBox.Show(id);
            lastID = Int32.Parse(id);
            //MessageBox.Show(lastID.ToString());

        }
    }
}

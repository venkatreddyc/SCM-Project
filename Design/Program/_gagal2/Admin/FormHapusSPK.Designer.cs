﻿namespace Admin
{
    partial class FormHapusSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cboSPK = new System.Windows.Forms.ComboBox();
            this.txtStatusHapus = new System.Windows.Forms.TextBox();
            this.btnHapus = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cari No SPK : ";
            // 
            // cboSPK
            // 
            this.cboSPK.FormattingEnabled = true;
            this.cboSPK.Location = new System.Drawing.Point(187, 77);
            this.cboSPK.Name = "cboSPK";
            this.cboSPK.Size = new System.Drawing.Size(219, 24);
            this.cboSPK.TabIndex = 1;
            // 
            // txtStatusHapus
            // 
            this.txtStatusHapus.Location = new System.Drawing.Point(75, 127);
            this.txtStatusHapus.Name = "txtStatusHapus";
            this.txtStatusHapus.Size = new System.Drawing.Size(331, 22);
            this.txtStatusHapus.TabIndex = 2;
            // 
            // btnHapus
            // 
            this.btnHapus.Location = new System.Drawing.Point(433, 126);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(75, 23);
            this.btnHapus.TabIndex = 3;
            this.btnHapus.Text = "HAPUS";
            this.btnHapus.UseVisualStyleBackColor = true;
            // 
            // FormHapusSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 252);
            this.Controls.Add(this.btnHapus);
            this.Controls.Add(this.txtStatusHapus);
            this.Controls.Add(this.cboSPK);
            this.Controls.Add(this.label1);
            this.Name = "FormHapusSPK";
            this.Text = "FormHapusSPK";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboSPK;
        private System.Windows.Forms.TextBox txtStatusHapus;
        private System.Windows.Forms.Button btnHapus;
    }
}
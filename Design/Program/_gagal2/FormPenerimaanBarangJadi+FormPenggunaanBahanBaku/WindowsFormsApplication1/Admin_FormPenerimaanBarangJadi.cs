﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Admin_FormPenerimaanBarangJadi : Form
    {
        string admin_username;
        string admin_kategori;
        string data;
        public Admin_FormPenerimaanBarangJadi()
        {
            InitializeComponent();
        }

        private void Admin_FormPenerimaanBarangJadi_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            
            labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            labelUser.Text = admin_username + " - " + admin_kategori;

            string loadstring = @"server=localhost;database=scm;userid=root;password=;";
            MySqlConnection conDataBase = new MySqlConnection(loadstring);
            string sql = "SELECT * FROM spk";
            MySqlConnection MyConn2 = new MySqlConnection(loadstring);
            MySqlCommand MyCommand2 = new MySqlCommand(sql, MyConn2);
            MySqlDataReader MyReader2;
            try
            {
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                    string namamesin = MyReader2.GetString("noSPK");
                    comboBoxSPK.Items.Add(namamesin);
                }
                //MessageBox.Show("Import Mesin berhasil");
                //comboBoxSPK.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        private void buttonKembali_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonCetak_Click(object sender, EventArgs e)
        {
            //string[] concat = data.Split('-');
            //string nama = concat[0];


            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("Penerimaan Barang Jadi.pdf", FileMode.Create));
            doc.Open();
            //Paragraph p = new Paragraph("Nama : " + nama+"\n" + "Alamat : " + concat[1]+"\n");
            Paragraph p = new Paragraph("aaa");
            doc.Add(p);
            doc.Close();
            

        }
        
        private void comboBoxSPK_SelectedValueChanged(object sender, EventArgs e)
        {
            var loadString = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadString))
            {
                connection.Open();
                var query1 = "SELECT c.nama as nama,c.alamat as alamat,s.kodeProduk as kode,s.biaya as biaya,fp.pembayaranSatu as pembayaranSatu,fp.pembayaranDua as pembayaranDua,fp.kekurangan as kekurangan,fp.caraPembayaranSatu as caraPembayaran from spk s INNER JOIN customer c on s.kodeCustomer=c.kodeCustomer INNER JOIN formpembayaran fp on s.noSPK=fp.noSPK WHERE s.noSPK='" + comboBoxSPK.SelectedItem.ToString() + "'";
                using (var command1 = new MySqlCommand(query1, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        string kode;
                        while (reader1.Read())
                        {
                            data = reader1.GetString("nama") + "-" + reader1.GetString("alamat") + "-" + reader1.GetString("kode") + "-" + reader1.GetString("biaya") + "-" + reader1.GetString("pembayaranSatu") + "-";

                            listBoxInfo.Items.Add("Nama : " + reader1.GetString("nama"));
                            listBoxInfo.Items.Add("Alamat : " + reader1.GetString("alamat"));
                            listBoxInfo.Items.Add("Kode Produk : " + reader1.GetString("kode"));
                            listBoxInfo.Items.Add("Total Harga : " + reader1.GetString("biaya"));
                            listBoxInfo.Items.Add("Pembayaran1 : " + reader1.GetString("pembayaranSatu"));
                            try
                            {
                                if (reader1.GetString("pembayaranDua") == "NULL")
                                {
                                    listBoxInfo.Items.Add("Pembayaran2 : -");
                                    data += reader1.GetString("Tidak Ada Pembayaran Kedua" + "-");
                                }
                                else
                                {
                                    listBoxInfo.Items.Add("Pembayaran2 : " + reader1.GetString("pembayaranDua"));
                                    data += reader1.GetString("pembayaranDua") + "-";
                                }
                            }
                            catch
                            {
                                listBoxInfo.Items.Add("Pembayaran2 : -");
                            }


                            listBoxInfo.Items.Add("Kekurangan : " + reader1.GetString("kekurangan"));
                            data += reader1.GetString("kekurangan") + "-" + reader1.GetString("caraPembayaran") + "-" + dateTimePickerTanggal.Value;
                            listBoxInfo.Items.Add("Cara pembayaran : " + reader1.GetString("caraPembayaran"));
                            listBoxInfo.Items.Add("Tanggal Penerimaan : " + dateTimePickerTanggal.Value);
                        }
                    }
                }
            }
        }

        private void comboBoxSPK_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}

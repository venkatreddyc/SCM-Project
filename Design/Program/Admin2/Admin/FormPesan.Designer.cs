﻿namespace Admin
{
    partial class FormPesan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelkode = new System.Windows.Forms.Label();
            this.labelTgl = new System.Windows.Forms.Label();
            this.labelJam = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonKembali = new System.Windows.Forms.Button();
            this.buttonPesan = new System.Windows.Forms.Button();
            this.buttonHapus = new System.Windows.Forms.Button();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.buttonTambahBahan = new System.Windows.Forms.Button();
            this.textBoxKeterangan = new System.Windows.Forms.TextBox();
            this.textBoxJumlah = new System.Windows.Forms.TextBox();
            this.comboBoxID = new System.Windows.Forms.ComboBox();
            this.comboBoxNoSpk = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelkode
            // 
            this.labelkode.AutoSize = true;
            this.labelkode.Location = new System.Drawing.Point(446, -28);
            this.labelkode.Name = "labelkode";
            this.labelkode.Size = new System.Drawing.Size(0, 13);
            this.labelkode.TabIndex = 59;
            this.labelkode.Visible = false;
            // 
            // labelTgl
            // 
            this.labelTgl.AutoSize = true;
            this.labelTgl.BackColor = System.Drawing.Color.Transparent;
            this.labelTgl.Location = new System.Drawing.Point(44, 384);
            this.labelTgl.Name = "labelTgl";
            this.labelTgl.Size = new System.Drawing.Size(22, 13);
            this.labelTgl.TabIndex = 57;
            this.labelTgl.Text = "Tgl";
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.BackColor = System.Drawing.Color.Transparent;
            this.labelJam.Location = new System.Drawing.Point(59, 363);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(26, 13);
            this.labelJam.TabIndex = 58;
            this.labelJam.Text = "Jam";
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(518, -44);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(29, 13);
            this.labelUser.TabIndex = 56;
            this.labelUser.Text = "User";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(93, 194);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(532, 102);
            this.dataGridView1.TabIndex = 55;
            // 
            // buttonKembali
            // 
            this.buttonKembali.Location = new System.Drawing.Point(567, 358);
            this.buttonKembali.Name = "buttonKembali";
            this.buttonKembali.Size = new System.Drawing.Size(75, 23);
            this.buttonKembali.TabIndex = 54;
            this.buttonKembali.Text = "KEMBALI";
            this.buttonKembali.UseVisualStyleBackColor = true;
            this.buttonKembali.Click += new System.EventHandler(this.buttonKembali_Click_1);
            // 
            // buttonPesan
            // 
            this.buttonPesan.Location = new System.Drawing.Point(214, 352);
            this.buttonPesan.Name = "buttonPesan";
            this.buttonPesan.Size = new System.Drawing.Size(213, 35);
            this.buttonPesan.TabIndex = 53;
            this.buttonPesan.Text = "PESAN";
            this.buttonPesan.UseVisualStyleBackColor = true;
            this.buttonPesan.UseWaitCursor = true;
            // 
            // buttonHapus
            // 
            this.buttonHapus.Location = new System.Drawing.Point(567, 158);
            this.buttonHapus.Name = "buttonHapus";
            this.buttonHapus.Size = new System.Drawing.Size(75, 23);
            this.buttonHapus.TabIndex = 52;
            this.buttonHapus.Text = "HAPUS";
            this.buttonHapus.UseVisualStyleBackColor = true;
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(474, 159);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmit.TabIndex = 51;
            this.buttonSubmit.Text = "SUBMIT";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            // 
            // buttonTambahBahan
            // 
            this.buttonTambahBahan.Location = new System.Drawing.Point(494, 73);
            this.buttonTambahBahan.Name = "buttonTambahBahan";
            this.buttonTambahBahan.Size = new System.Drawing.Size(120, 41);
            this.buttonTambahBahan.TabIndex = 50;
            this.buttonTambahBahan.Text = "TAMBAH BAHAN";
            this.buttonTambahBahan.UseVisualStyleBackColor = true;
            // 
            // textBoxKeterangan
            // 
            this.textBoxKeterangan.Location = new System.Drawing.Point(287, 160);
            this.textBoxKeterangan.Name = "textBoxKeterangan";
            this.textBoxKeterangan.Size = new System.Drawing.Size(161, 20);
            this.textBoxKeterangan.TabIndex = 49;
            // 
            // textBoxJumlah
            // 
            this.textBoxJumlah.Location = new System.Drawing.Point(287, 132);
            this.textBoxJumlah.Name = "textBoxJumlah";
            this.textBoxJumlah.Size = new System.Drawing.Size(88, 20);
            this.textBoxJumlah.TabIndex = 48;
            // 
            // comboBoxID
            // 
            this.comboBoxID.FormattingEnabled = true;
            this.comboBoxID.Location = new System.Drawing.Point(348, 84);
            this.comboBoxID.Name = "comboBoxID";
            this.comboBoxID.Size = new System.Drawing.Size(121, 21);
            this.comboBoxID.TabIndex = 47;
            // 
            // comboBoxNoSpk
            // 
            this.comboBoxNoSpk.FormattingEnabled = true;
            this.comboBoxNoSpk.Location = new System.Drawing.Point(348, 56);
            this.comboBoxNoSpk.Name = "comboBoxNoSpk";
            this.comboBoxNoSpk.Size = new System.Drawing.Size(121, 21);
            this.comboBoxNoSpk.TabIndex = 46;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(202, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Keterangan:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(202, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Jumlah:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(202, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 13);
            this.label5.TabIndex = 43;
            this.label5.Text = "ID - NAMA BAHAN BAKU:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(202, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "No. SPK - KODE PRODUK:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(607, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 60;
            this.label3.Text = "label3";
            // 
            // FormPesan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 439);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelkode);
            this.Controls.Add(this.labelTgl);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buttonKembali);
            this.Controls.Add(this.buttonPesan);
            this.Controls.Add(this.buttonHapus);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.buttonTambahBahan);
            this.Controls.Add(this.textBoxKeterangan);
            this.Controls.Add(this.textBoxJumlah);
            this.Controls.Add(this.comboBoxID);
            this.Controls.Add(this.comboBoxNoSpk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Name = "FormPesan";
            this.Text = "FormPesan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPesan_FormClosing);
            this.Load += new System.EventHandler(this.FormPesan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelkode;
        private System.Windows.Forms.Label labelTgl;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonKembali;
        private System.Windows.Forms.Button buttonPesan;
        private System.Windows.Forms.Button buttonHapus;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Button buttonTambahBahan;
        private System.Windows.Forms.TextBox textBoxKeterangan;
        private System.Windows.Forms.TextBox textBoxJumlah;
        private System.Windows.Forms.ComboBox comboBoxID;
        private System.Windows.Forms.ComboBox comboBoxNoSpk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
    }
}
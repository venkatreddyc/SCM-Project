﻿namespace Admin
{
    partial class FormStatusSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboSPK = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lboStatusSPK = new System.Windows.Forms.ListBox();
            this.btnCetak = new System.Windows.Forms.Button();
            this.btnKembali = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cboSPK
            // 
            this.cboSPK.FormattingEnabled = true;
            this.cboSPK.Location = new System.Drawing.Point(168, 29);
            this.cboSPK.Name = "cboSPK";
            this.cboSPK.Size = new System.Drawing.Size(219, 24);
            this.cboSPK.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Cari No SPK : ";
            // 
            // lboStatusSPK
            // 
            this.lboStatusSPK.FormattingEnabled = true;
            this.lboStatusSPK.ItemHeight = 16;
            this.lboStatusSPK.Location = new System.Drawing.Point(56, 85);
            this.lboStatusSPK.Name = "lboStatusSPK";
            this.lboStatusSPK.Size = new System.Drawing.Size(331, 276);
            this.lboStatusSPK.TabIndex = 4;
            // 
            // btnCetak
            // 
            this.btnCetak.Location = new System.Drawing.Point(231, 374);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(75, 23);
            this.btnCetak.TabIndex = 5;
            this.btnCetak.Text = "CETAK";
            this.btnCetak.UseVisualStyleBackColor = true;
            // 
            // btnKembali
            // 
            this.btnKembali.Location = new System.Drawing.Point(312, 374);
            this.btnKembali.Name = "btnKembali";
            this.btnKembali.Size = new System.Drawing.Size(75, 23);
            this.btnKembali.TabIndex = 6;
            this.btnKembali.Text = "KEMBALI";
            this.btnKembali.UseVisualStyleBackColor = true;
            // 
            // FormStatusSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 409);
            this.Controls.Add(this.btnKembali);
            this.Controls.Add(this.btnCetak);
            this.Controls.Add(this.lboStatusSPK);
            this.Controls.Add(this.cboSPK);
            this.Controls.Add(this.label1);
            this.Name = "FormStatusSPK";
            this.Text = "FormStatusSPK";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboSPK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lboStatusSPK;
        private System.Windows.Forms.Button btnCetak;
        private System.Windows.Forms.Button btnKembali;
    }
}
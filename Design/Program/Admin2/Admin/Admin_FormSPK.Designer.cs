﻿namespace Admin
{
    partial class Admin_FormSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBuatSPK = new System.Windows.Forms.Button();
            this.btnStatusSPK = new System.Windows.Forms.Button();
            this.btnHapusSPK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonBuatSPK
            // 
            this.buttonBuatSPK.Location = new System.Drawing.Point(44, 125);
            this.buttonBuatSPK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonBuatSPK.Name = "buttonBuatSPK";
            this.buttonBuatSPK.Size = new System.Drawing.Size(124, 86);
            this.buttonBuatSPK.TabIndex = 3;
            this.buttonBuatSPK.Text = "BUAT SPK";
            this.buttonBuatSPK.UseVisualStyleBackColor = true;
            // 
            // btnStatusSPK
            // 
            this.btnStatusSPK.Location = new System.Drawing.Point(235, 125);
            this.btnStatusSPK.Margin = new System.Windows.Forms.Padding(4);
            this.btnStatusSPK.Name = "btnStatusSPK";
            this.btnStatusSPK.Size = new System.Drawing.Size(124, 86);
            this.btnStatusSPK.TabIndex = 4;
            this.btnStatusSPK.Text = "STATUS SPK";
            this.btnStatusSPK.UseVisualStyleBackColor = true;
            this.btnStatusSPK.Click += new System.EventHandler(this.btnStatusSPK_Click);
            // 
            // btnHapusSPK
            // 
            this.btnHapusSPK.Location = new System.Drawing.Point(413, 125);
            this.btnHapusSPK.Margin = new System.Windows.Forms.Padding(4);
            this.btnHapusSPK.Name = "btnHapusSPK";
            this.btnHapusSPK.Size = new System.Drawing.Size(124, 86);
            this.btnHapusSPK.TabIndex = 5;
            this.btnHapusSPK.Text = "HAPUS SPK";
            this.btnHapusSPK.UseVisualStyleBackColor = true;
            this.btnHapusSPK.Click += new System.EventHandler(this.btnHapusSPK_Click);
            // 
            // Admin_FormSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 355);
            this.Controls.Add(this.btnHapusSPK);
            this.Controls.Add(this.btnStatusSPK);
            this.Controls.Add(this.buttonBuatSPK);
            this.Name = "Admin_FormSPK";
            this.Text = "Admin_FormSPK";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonBuatSPK;
        private System.Windows.Forms.Button btnStatusSPK;
        private System.Windows.Forms.Button btnHapusSPK;
    }
}
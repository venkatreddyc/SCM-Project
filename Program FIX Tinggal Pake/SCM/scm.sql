-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03 Jun 2018 pada 16.37
-- Versi Server: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scm`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahanbaku`
--

CREATE TABLE `bahanbaku` (
  `kodeBahanBaku` char(5) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `satuan` varchar(45) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `bahanbaku`
--

INSERT INTO `bahanbaku` (`kodeBahanBaku`, `nama`, `satuan`, `stok`) VALUES
('b0001', 'boneka', 'kilo', 100),
('b0002', 'sutil', 'pcs', 100);

-- --------------------------------------------------------

--
-- Struktur dari tabel `buktipengiriman`
--

CREATE TABLE `buktipengiriman` (
  `kodePengiriman` varchar(45) NOT NULL,
  `tanggal` date NOT NULL,
  `kodeCustomer` char(5) NOT NULL,
  `noSPK` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `buktipengiriman`
--

INSERT INTO `buktipengiriman` (`kodePengiriman`, `tanggal`, `kodeCustomer`, `noSPK`) VALUES
('KP001', '2018-04-26', '00001', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `kodeCustomer` char(5) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `alamat` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`kodeCustomer`, `nama`, `alamat`) VALUES
('00001', 'joseph', 'wonorejo'),
('00002', 'deni', 'mejoyo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detilbahanbakusupplier`
--

CREATE TABLE `detilbahanbakusupplier` (
  `kodeBahanBaku` char(5) NOT NULL,
  `kodeSupplier` char(2) NOT NULL,
  `hargaSatuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `detilbahanbakusupplier`
--

INSERT INTO `detilbahanbakusupplier` (`kodeBahanBaku`, `kodeSupplier`, `hargaSatuan`) VALUES
('b0001', 'S1', 10000),
('b0001', 'S2', 70000),
('b0002', 'S1', 20000),
('b0002', 'S2', 40000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `formjadwal`
--

CREATE TABLE `formjadwal` (
  `kodeJadwal` char(11) NOT NULL,
  `tanggalSelesai` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `noSPK` varchar(45) NOT NULL,
  `tenagaKerjaSatu` varchar(45) DEFAULT NULL,
  `tenagaKerjaDua` varchar(45) DEFAULT NULL,
  `tenagaKerjaTiga` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `formjadwal`
--

INSERT INTO `formjadwal` (`kodeJadwal`, `tanggalSelesai`, `status`, `noSPK`, `tenagaKerjaSatu`, `tenagaKerjaDua`, `tenagaKerjaTiga`) VALUES
('J001', '2018-04-20', 0, '1', 'a', 'b', 'c');

-- --------------------------------------------------------

--
-- Struktur dari tabel `formpembayaran`
--

CREATE TABLE `formpembayaran` (
  `kodePembayaran` char(12) NOT NULL,
  `pembayaranSatu` int(11) NOT NULL,
  `tanggalPembayaranSatu` date NOT NULL,
  `caraPembayaranSatu` varchar(45) NOT NULL,
  `pembayaranDua` int(11) DEFAULT NULL,
  `tanggalPembayaranDua` date DEFAULT NULL,
  `CaraPembayaranDua` varchar(45) DEFAULT NULL,
  `kekurangan` int(11) NOT NULL,
  `noSPK` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `formpembayaran`
--

INSERT INTO `formpembayaran` (`kodePembayaran`, `pembayaranSatu`, `tanggalPembayaranSatu`, `caraPembayaranSatu`, `pembayaranDua`, `tanggalPembayaranDua`, `CaraPembayaranDua`, `kekurangan`, `noSPK`) VALUES
('pem01', 1000000, '2018-04-12', 'tunai', NULL, NULL, NULL, 2000000, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `formpembelian`
--

CREATE TABLE `formpembelian` (
  `kodePembelian` char(13) NOT NULL,
  `tanggal` date NOT NULL,
  `kodeSupplier` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `formpembelian`
--

INSERT INTO `formpembelian` (`kodePembelian`, `tanggal`, `kodeSupplier`) VALUES
('pembelian01', '2018-04-12', 'S1'),
('Pembelian02', '2018-05-21', 'S1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `formpembeliandetil`
--

CREATE TABLE `formpembeliandetil` (
  `kodePembelian` char(13) NOT NULL,
  `kodeBahanBaku` char(5) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `tanggalDatang` datetime DEFAULT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `formpembeliandetil`
--

INSERT INTO `formpembeliandetil` (`kodePembelian`, `kodeBahanBaku`, `jumlah`, `subtotal`, `status`, `tanggalDatang`, `total`) VALUES
('pembelian01', 'b0001', 5, 500000, 1, '2018-05-22 02:22:59', 500000),
('pembelian01', 'b0002', 5, 10000, 1, '2018-05-22 02:23:49', 10000),
('Pembelian02', 'b0001', 5, 500000, 1, '2018-05-22 02:22:59', 22);

-- --------------------------------------------------------

--
-- Struktur dari tabel `formpemesanan`
--

CREATE TABLE `formpemesanan` (
  `kodePemesanan` char(13) NOT NULL,
  `noSPK` varchar(45) NOT NULL,
  `tanggalPesan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `formpemesanan`
--

INSERT INTO `formpemesanan` (`kodePemesanan`, `noSPK`, `tanggalPesan`) VALUES
('pemesanan01', '1', '2018-04-13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `formpemesanandetil`
--

CREATE TABLE `formpemesanandetil` (
  `kodeBahanBaku` char(5) NOT NULL,
  `kodePemesanan` char(13) NOT NULL,
  `noSPK` varchar(45) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `statusKirim` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `formpemesanandetil`
--

INSERT INTO `formpemesanandetil` (`kodeBahanBaku`, `kodePemesanan`, `noSPK`, `jumlah`, `keterangan`, `statusKirim`) VALUES
('b0001', 'pemesanan01', '1', 5, NULL, 1),
('b0002', 'pemesanan01', '1', 100, NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `formpenggunaandetil`
--

CREATE TABLE `formpenggunaandetil` (
  `noSPK` varchar(45) NOT NULL,
  `kodeBahanBaku` char(5) NOT NULL,
  `jumlahKeluar` int(11) NOT NULL,
  `sisaPenggunaan` int(11) NOT NULL,
  `tanggalKeluar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `formpenggunaandetil`
--

INSERT INTO `formpenggunaandetil` (`noSPK`, `kodeBahanBaku`, `jumlahKeluar`, `sisaPenggunaan`, `tanggalKeluar`) VALUES
('1', 'b0001', 3, 2, '2018-04-14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `kodeJabatan` char(2) NOT NULL,
  `namaJabatan` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`kodeJabatan`, `namaJabatan`) VALUES
('J1', 'Owner'),
('J2', 'Admin'),
('J3', 'Finance'),
('J4', 'Gudang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwalmesin`
--

CREATE TABLE `jadwalmesin` (
  `kodeJadwal` char(11) NOT NULL,
  `noSPK` varchar(45) NOT NULL,
  `noMesin` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `jadwalmesin`
--

INSERT INTO `jadwalmesin` (`kodeJadwal`, `noSPK`, `noMesin`) VALUES
('J001', '1', 'm01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mesin`
--

CREATE TABLE `mesin` (
  `noMesin` char(3) NOT NULL,
  `nama` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `mesin`
--

INSERT INTO `mesin` (`noMesin`, `nama`) VALUES
('123', 'deni'),
('m01', 'penggiling');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notapenerimaan`
--

CREATE TABLE `notapenerimaan` (
  `kodePenerimaan` char(13) NOT NULL,
  `tanggalTerima` date NOT NULL,
  `kodeCustomer` char(5) NOT NULL,
  `keterangan` varchar(45) NOT NULL,
  `kodePembayaran` char(12) NOT NULL,
  `noSPK` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `notapenerimaan`
--

INSERT INTO `notapenerimaan` (`kodePenerimaan`, `tanggalTerima`, `kodeCustomer`, `keterangan`, `kodePembayaran`, `noSPK`) VALUES
('penerimaan01', '2018-04-26', '00001', 'terkirim', 'pem01', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `progressproduksi`
--

CREATE TABLE `progressproduksi` (
  `noMesin` char(3) NOT NULL,
  `noSPK` varchar(45) NOT NULL,
  `tanggal_proses` date NOT NULL,
  `namaPekerjaan` varchar(45) NOT NULL,
  `jamKerja` int(11) NOT NULL,
  `Hasil` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `progressproduksi`
--

INSERT INTO `progressproduksi` (`noMesin`, `noSPK`, `tanggal_proses`, `namaPekerjaan`, `jamKerja`, `Hasil`) VALUES
('m01', '1', '2018-04-16', 'mesukno narkoba', 2, 'boneka narkoba');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spk`
--

CREATE TABLE `spk` (
  `noSPK` varchar(45) NOT NULL,
  `tanggal` datetime NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `biaya` int(11) NOT NULL,
  `lama` int(11) NOT NULL,
  `syarat` varchar(45) NOT NULL,
  `status` int(1) NOT NULL,
  `komentar` varchar(255) DEFAULT NULL,
  `kodeCustomer` char(5) NOT NULL,
  `idUser` char(4) NOT NULL,
  `kodeProduk` char(11) DEFAULT NULL,
  `tanggalMulai` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `spk`
--

INSERT INTO `spk` (`noSPK`, `tanggal`, `pekerjaan`, `biaya`, `lama`, `syarat`, `status`, `komentar`, `kodeCustomer`, `idUser`, `kodeProduk`, `tanggalMulai`) VALUES
('1', '2018-04-12 03:15:10', 'macul sawah', 3000000, 30, 'punya cangkul', 0, '-', '00001', 'u001', 'P1204201801', '2018-04-19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `kodeSupplier` char(3) NOT NULL,
  `namaSupplier` varchar(45) NOT NULL,
  `alamat` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`kodeSupplier`, `namaSupplier`, `alamat`) VALUES
('S1', 'joijedolls', 'wonorejo'),
('S2', 'jokitchenware', '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `userti`
--

CREATE TABLE `userti` (
  `idUser` char(4) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `alamat` varchar(45) NOT NULL,
  `telepon` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `kodeJabatan` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `userti`
--

INSERT INTO `userti` (`idUser`, `nama`, `alamat`, `telepon`, `username`, `password`, `kodeJabatan`) VALUES
('u001', 'ming', 'kutisari', '123', 'fleming', '0000', 'J1'),
('u002', 'steven', 'rungkut', '12345', 'ming1', '1234', 'J2'),
('u003', 'joseph', 'wonorejo', '0813', 'jo7', 'lala', 'J3'),
('u004', 'Deni', 'Rmu', '0811', 'deni7', 'ega', 'J4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahanbaku`
--
ALTER TABLE `bahanbaku`
  ADD PRIMARY KEY (`kodeBahanBaku`);

--
-- Indexes for table `buktipengiriman`
--
ALTER TABLE `buktipengiriman`
  ADD PRIMARY KEY (`kodePengiriman`,`kodeCustomer`,`noSPK`),
  ADD KEY `fk_bukti_pengiriman_pemesan1_idx` (`kodeCustomer`),
  ADD KEY `fk_bukti_pengiriman_spk1_idx` (`noSPK`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`kodeCustomer`);

--
-- Indexes for table `detilbahanbakusupplier`
--
ALTER TABLE `detilbahanbakusupplier`
  ADD PRIMARY KEY (`kodeBahanBaku`,`kodeSupplier`),
  ADD KEY `fk_bahanbaku_has_supplier_supplier1_idx` (`kodeSupplier`),
  ADD KEY `fk_bahanbaku_has_supplier_bahanbaku1_idx` (`kodeBahanBaku`);

--
-- Indexes for table `formjadwal`
--
ALTER TABLE `formjadwal`
  ADD PRIMARY KEY (`kodeJadwal`,`noSPK`),
  ADD KEY `fk_jadwal_spk1_idx` (`noSPK`);

--
-- Indexes for table `formpembayaran`
--
ALTER TABLE `formpembayaran`
  ADD PRIMARY KEY (`kodePembayaran`,`noSPK`),
  ADD KEY `fk_formpembayaran_spk1_idx` (`noSPK`);

--
-- Indexes for table `formpembelian`
--
ALTER TABLE `formpembelian`
  ADD PRIMARY KEY (`kodePembelian`),
  ADD KEY `fk_form_pemesanan_supplier1_idx` (`kodeSupplier`);

--
-- Indexes for table `formpembeliandetil`
--
ALTER TABLE `formpembeliandetil`
  ADD PRIMARY KEY (`kodePembelian`,`kodeBahanBaku`),
  ADD KEY `fk_formpemesanan_has_bahanbaku_bahanbaku1_idx` (`kodeBahanBaku`),
  ADD KEY `fk_formpemesanan_has_bahanbaku_formpemesanan1_idx` (`kodePembelian`);

--
-- Indexes for table `formpemesanan`
--
ALTER TABLE `formpemesanan`
  ADD PRIMARY KEY (`kodePemesanan`,`noSPK`),
  ADD KEY `fk_formpemesanan_spk1_idx` (`noSPK`);

--
-- Indexes for table `formpemesanandetil`
--
ALTER TABLE `formpemesanandetil`
  ADD PRIMARY KEY (`kodeBahanBaku`,`kodePemesanan`,`noSPK`),
  ADD KEY `fk_bahanbaku_has_formpemesanan_formpemesanan1_idx` (`kodePemesanan`,`noSPK`),
  ADD KEY `fk_bahanbaku_has_formpemesanan_bahanbaku1_idx` (`kodeBahanBaku`);

--
-- Indexes for table `formpenggunaandetil`
--
ALTER TABLE `formpenggunaandetil`
  ADD PRIMARY KEY (`noSPK`,`kodeBahanBaku`),
  ADD KEY `fk_formpenggunaan_has_bahanbaku_bahanbaku1_idx` (`kodeBahanBaku`),
  ADD KEY `fk_formpenggunaandetil_spk1_idx` (`noSPK`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kodeJabatan`);

--
-- Indexes for table `jadwalmesin`
--
ALTER TABLE `jadwalmesin`
  ADD PRIMARY KEY (`kodeJadwal`,`noSPK`,`noMesin`),
  ADD KEY `fk_formjadwal_has_mesin_mesin1_idx` (`noMesin`),
  ADD KEY `fk_formjadwal_has_mesin_formjadwal1_idx` (`kodeJadwal`,`noSPK`);

--
-- Indexes for table `mesin`
--
ALTER TABLE `mesin`
  ADD PRIMARY KEY (`noMesin`);

--
-- Indexes for table `notapenerimaan`
--
ALTER TABLE `notapenerimaan`
  ADD PRIMARY KEY (`kodePenerimaan`,`kodeCustomer`,`kodePembayaran`,`noSPK`),
  ADD KEY `fk_nota_penerimaan_client1_idx` (`kodeCustomer`),
  ADD KEY `fk_nota_penerimaan_formpembayaran1_idx` (`kodePembayaran`,`noSPK`);

--
-- Indexes for table `progressproduksi`
--
ALTER TABLE `progressproduksi`
  ADD PRIMARY KEY (`noMesin`,`noSPK`),
  ADD KEY `fk_mesin_has_formprogressproduksi_mesin1_idx` (`noMesin`),
  ADD KEY `fk_progressproduksi_spk1_idx` (`noSPK`);

--
-- Indexes for table `spk`
--
ALTER TABLE `spk`
  ADD PRIMARY KEY (`noSPK`),
  ADD KEY `fk_spk_customer1_idx` (`kodeCustomer`),
  ADD KEY `fk_spk_userTI1_idx` (`idUser`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kodeSupplier`);

--
-- Indexes for table `userti`
--
ALTER TABLE `userti`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `fk_userTI_jabatan1_idx` (`kodeJabatan`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `buktipengiriman`
--
ALTER TABLE `buktipengiriman`
  ADD CONSTRAINT `fk_bukti_pengiriman_pemesan1` FOREIGN KEY (`kodeCustomer`) REFERENCES `customer` (`kodeCustomer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_bukti_pengiriman_spk1` FOREIGN KEY (`noSPK`) REFERENCES `spk` (`noSPK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `detilbahanbakusupplier`
--
ALTER TABLE `detilbahanbakusupplier`
  ADD CONSTRAINT `fk_bahanbaku_has_supplier_bahanbaku1` FOREIGN KEY (`kodeBahanBaku`) REFERENCES `bahanbaku` (`kodeBahanBaku`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_bahanbaku_has_supplier_supplier1` FOREIGN KEY (`kodeSupplier`) REFERENCES `supplier` (`kodeSupplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `formjadwal`
--
ALTER TABLE `formjadwal`
  ADD CONSTRAINT `fk_jadwal_spk1` FOREIGN KEY (`noSPK`) REFERENCES `spk` (`noSPK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `formpembayaran`
--
ALTER TABLE `formpembayaran`
  ADD CONSTRAINT `fk_formpembayaran_spk1` FOREIGN KEY (`noSPK`) REFERENCES `spk` (`noSPK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `formpembelian`
--
ALTER TABLE `formpembelian`
  ADD CONSTRAINT `fk_form_pemesanan_supplier1` FOREIGN KEY (`kodeSupplier`) REFERENCES `supplier` (`kodeSupplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `formpembeliandetil`
--
ALTER TABLE `formpembeliandetil`
  ADD CONSTRAINT `fk_formpemesanan_has_bahanbaku_bahanbaku1` FOREIGN KEY (`kodeBahanBaku`) REFERENCES `bahanbaku` (`kodeBahanBaku`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_formpemesanan_has_bahanbaku_formpemesanan1` FOREIGN KEY (`kodePembelian`) REFERENCES `formpembelian` (`kodePembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `formpemesanan`
--
ALTER TABLE `formpemesanan`
  ADD CONSTRAINT `fk_formpemesanan_spk1` FOREIGN KEY (`noSPK`) REFERENCES `spk` (`noSPK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `formpemesanandetil`
--
ALTER TABLE `formpemesanandetil`
  ADD CONSTRAINT `fk_bahanbaku_has_formpemesanan_bahanbaku1` FOREIGN KEY (`kodeBahanBaku`) REFERENCES `bahanbaku` (`kodeBahanBaku`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_bahanbaku_has_formpemesanan_formpemesanan1` FOREIGN KEY (`kodePemesanan`,`noSPK`) REFERENCES `formpemesanan` (`kodePemesanan`, `noSPK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `formpenggunaandetil`
--
ALTER TABLE `formpenggunaandetil`
  ADD CONSTRAINT `fk_formpenggunaan_has_bahanbaku_bahanbaku1` FOREIGN KEY (`kodeBahanBaku`) REFERENCES `bahanbaku` (`kodeBahanBaku`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_formpenggunaandetil_spk1` FOREIGN KEY (`noSPK`) REFERENCES `spk` (`noSPK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `jadwalmesin`
--
ALTER TABLE `jadwalmesin`
  ADD CONSTRAINT `fk_formjadwal_has_mesin_formjadwal1` FOREIGN KEY (`kodeJadwal`,`noSPK`) REFERENCES `formjadwal` (`kodeJadwal`, `noSPK`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_formjadwal_has_mesin_mesin1` FOREIGN KEY (`noMesin`) REFERENCES `mesin` (`noMesin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `notapenerimaan`
--
ALTER TABLE `notapenerimaan`
  ADD CONSTRAINT `fk_nota_penerimaan_client1` FOREIGN KEY (`kodeCustomer`) REFERENCES `customer` (`kodeCustomer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_nota_penerimaan_formpembayaran1` FOREIGN KEY (`kodePembayaran`,`noSPK`) REFERENCES `formpembayaran` (`kodePembayaran`, `noSPK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `progressproduksi`
--
ALTER TABLE `progressproduksi`
  ADD CONSTRAINT `fk_mesin_has_formprogressproduksi_mesin1` FOREIGN KEY (`noMesin`) REFERENCES `mesin` (`noMesin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_progressproduksi_spk1` FOREIGN KEY (`noSPK`) REFERENCES `spk` (`noSPK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `spk`
--
ALTER TABLE `spk`
  ADD CONSTRAINT `fk_spk_customer1` FOREIGN KEY (`kodeCustomer`) REFERENCES `customer` (`kodeCustomer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_spk_userTI1` FOREIGN KEY (`idUser`) REFERENCES `userti` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `userti`
--
ALTER TABLE `userti`
  ADD CONSTRAINT `fk_userTI_jabatan1` FOREIGN KEY (`kodeJabatan`) REFERENCES `jabatan` (`kodeJabatan`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

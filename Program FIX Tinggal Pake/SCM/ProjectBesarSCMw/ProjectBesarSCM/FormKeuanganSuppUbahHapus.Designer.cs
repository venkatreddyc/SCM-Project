﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganSuppUbahHapus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelHello = new System.Windows.Forms.Label();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.labelBahanBaku = new System.Windows.Forms.Label();
            this.labelRp = new System.Windows.Forms.Label();
            this.labelHarga = new System.Windows.Forms.Label();
            this.labelHargaSatuan = new System.Windows.Forms.Label();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonSimpan = new System.Windows.Forms.Button();
            this.labelDate = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelHello.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHello.Location = new System.Drawing.Point(421, 10);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(60, 19);
            this.labelHello.TabIndex = 4;
            this.labelHello.Text = "Hello  , ";
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelSupplier.Location = new System.Drawing.Point(206, 172);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(69, 13);
            this.labelSupplier.TabIndex = 8;
            this.labelSupplier.Text = "SUPPLIER : ";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(274, 169);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(145, 21);
            this.comboBox1.TabIndex = 9;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(274, 196);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(145, 21);
            this.comboBox2.TabIndex = 11;
            // 
            // labelBahanBaku
            // 
            this.labelBahanBaku.AutoSize = true;
            this.labelBahanBaku.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelBahanBaku.Location = new System.Drawing.Point(191, 199);
            this.labelBahanBaku.Name = "labelBahanBaku";
            this.labelBahanBaku.Size = new System.Drawing.Size(85, 13);
            this.labelBahanBaku.TabIndex = 10;
            this.labelBahanBaku.Text = "BAHAN BAKU : ";
            // 
            // labelRp
            // 
            this.labelRp.AutoSize = true;
            this.labelRp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelRp.Location = new System.Drawing.Point(274, 230);
            this.labelRp.Name = "labelRp";
            this.labelRp.Size = new System.Drawing.Size(24, 13);
            this.labelRp.TabIndex = 18;
            this.labelRp.Text = "Rp.";
            // 
            // labelHarga
            // 
            this.labelHarga.AutoSize = true;
            this.labelHarga.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelHarga.Location = new System.Drawing.Point(304, 230);
            this.labelHarga.Name = "labelHarga";
            this.labelHarga.Size = new System.Drawing.Size(13, 13);
            this.labelHarga.TabIndex = 17;
            this.labelHarga.Text = "0";
            // 
            // labelHargaSatuan
            // 
            this.labelHargaSatuan.AutoSize = true;
            this.labelHargaSatuan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelHargaSatuan.Location = new System.Drawing.Point(175, 230);
            this.labelHargaSatuan.Name = "labelHargaSatuan";
            this.labelHargaSatuan.Size = new System.Drawing.Size(101, 13);
            this.labelHargaSatuan.TabIndex = 16;
            this.labelHargaSatuan.Text = "HARGA SATUAN : ";
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLogin.FlatAppearance.BorderSize = 0;
            this.buttonLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Location = new System.Drawing.Point(502, 401);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(93, 40);
            this.buttonLogin.TabIndex = 31;
            this.buttonLogin.Text = "KEMBALI";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonSimpan
            // 
            this.buttonSimpan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonSimpan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSimpan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSimpan.Location = new System.Drawing.Point(194, 289);
            this.buttonSimpan.Name = "buttonSimpan";
            this.buttonSimpan.Size = new System.Drawing.Size(239, 49);
            this.buttonSimpan.TabIndex = 30;
            this.buttonSimpan.Text = "HAPUS";
            this.buttonSimpan.UseVisualStyleBackColor = false;
            this.buttonSimpan.Click += new System.EventHandler(this.buttonSimpan_Click);
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelDate.Location = new System.Drawing.Point(6, 423);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(30, 13);
            this.labelDate.TabIndex = 32;
            this.labelDate.Text = "Date";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormKeuanganSuppUbahHapus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Supp_Ubah_Hapus;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(598, 453);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.buttonSimpan);
            this.Controls.Add(this.labelRp);
            this.Controls.Add(this.labelHarga);
            this.Controls.Add(this.labelHargaSatuan);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.labelBahanBaku);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.labelHello);
            this.Name = "FormKeuanganSuppUbahHapus";
            this.Text = "FormKeuanganSuppUbahHapus";
            this.Load += new System.EventHandler(this.FormKeuanganSuppUbahHapus_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHello;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label labelBahanBaku;
        private System.Windows.Forms.Label labelRp;
        private System.Windows.Forms.Label labelHarga;
        private System.Windows.Forms.Label labelHargaSatuan;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonSimpan;
        public System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Timer timer1;
    }
}
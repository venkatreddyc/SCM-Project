﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganTransaksi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelHello = new System.Windows.Forms.Label();
            this.labelNoSpk = new System.Windows.Forms.Label();
            this.textBoxNoSpk = new System.Windows.Forms.TextBox();
            this.labelTanggal = new System.Windows.Forms.Label();
            this.labelInfoTanggal = new System.Windows.Forms.Label();
            this.labelInfoPemesan = new System.Windows.Forms.Label();
            this.labelPemesanan = new System.Windows.Forms.Label();
            this.labelInfoAlamat = new System.Windows.Forms.Label();
            this.labelAlamat = new System.Windows.Forms.Label();
            this.labelInfoTotal = new System.Windows.Forms.Label();
            this.labelTotalHarga = new System.Windows.Forms.Label();
            this.labelInfoPekerjaan = new System.Windows.Forms.Label();
            this.labelPekerjaan = new System.Windows.Forms.Label();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonInputPembayaran = new System.Windows.Forms.Button();
            this.labelDate = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelHello.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHello.Location = new System.Drawing.Point(439, 12);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(60, 19);
            this.labelHello.TabIndex = 20;
            this.labelHello.Text = "Hello  , ";
            // 
            // labelNoSpk
            // 
            this.labelNoSpk.AutoSize = true;
            this.labelNoSpk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelNoSpk.Location = new System.Drawing.Point(111, 103);
            this.labelNoSpk.Name = "labelNoSpk";
            this.labelNoSpk.Size = new System.Drawing.Size(53, 13);
            this.labelNoSpk.TabIndex = 21;
            this.labelNoSpk.Text = "NO. SPK:";
            // 
            // textBoxNoSpk
            // 
            this.textBoxNoSpk.Location = new System.Drawing.Point(172, 100);
            this.textBoxNoSpk.Name = "textBoxNoSpk";
            this.textBoxNoSpk.Size = new System.Drawing.Size(185, 20);
            this.textBoxNoSpk.TabIndex = 22;
            // 
            // labelTanggal
            // 
            this.labelTanggal.AutoSize = true;
            this.labelTanggal.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelTanggal.Location = new System.Drawing.Point(137, 205);
            this.labelTanggal.Name = "labelTanggal";
            this.labelTanggal.Size = new System.Drawing.Size(55, 13);
            this.labelTanggal.TabIndex = 30;
            this.labelTanggal.Text = "Tanggal : ";
            // 
            // labelInfoTanggal
            // 
            this.labelInfoTanggal.AutoSize = true;
            this.labelInfoTanggal.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelInfoTanggal.Location = new System.Drawing.Point(195, 205);
            this.labelInfoTanggal.Name = "labelInfoTanggal";
            this.labelInfoTanggal.Size = new System.Drawing.Size(46, 13);
            this.labelInfoTanggal.TabIndex = 31;
            this.labelInfoTanggal.Text = "Tanggal";
            // 
            // labelInfoPemesan
            // 
            this.labelInfoPemesan.AutoSize = true;
            this.labelInfoPemesan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelInfoPemesan.Location = new System.Drawing.Point(194, 232);
            this.labelInfoPemesan.Name = "labelInfoPemesan";
            this.labelInfoPemesan.Size = new System.Drawing.Size(78, 13);
            this.labelInfoPemesan.TabIndex = 33;
            this.labelInfoPemesan.Text = " Nama Pembeli";
            // 
            // labelPemesanan
            // 
            this.labelPemesanan.AutoSize = true;
            this.labelPemesanan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelPemesanan.Location = new System.Drawing.Point(132, 232);
            this.labelPemesanan.Name = "labelPemesanan";
            this.labelPemesanan.Size = new System.Drawing.Size(60, 13);
            this.labelPemesanan.TabIndex = 32;
            this.labelPemesanan.Text = "Pemesan : ";
            // 
            // labelInfoAlamat
            // 
            this.labelInfoAlamat.AutoSize = true;
            this.labelInfoAlamat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelInfoAlamat.Location = new System.Drawing.Point(194, 256);
            this.labelInfoAlamat.Name = "labelInfoAlamat";
            this.labelInfoAlamat.Size = new System.Drawing.Size(79, 13);
            this.labelInfoAlamat.TabIndex = 35;
            this.labelInfoAlamat.Text = "Alamat Pembeli";
            // 
            // labelAlamat
            // 
            this.labelAlamat.AutoSize = true;
            this.labelAlamat.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelAlamat.Location = new System.Drawing.Point(144, 256);
            this.labelAlamat.Name = "labelAlamat";
            this.labelAlamat.Size = new System.Drawing.Size(48, 13);
            this.labelAlamat.TabIndex = 34;
            this.labelAlamat.Text = "Alamat : ";
            // 
            // labelInfoTotal
            // 
            this.labelInfoTotal.AutoSize = true;
            this.labelInfoTotal.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelInfoTotal.Location = new System.Drawing.Point(195, 320);
            this.labelInfoTotal.Name = "labelInfoTotal";
            this.labelInfoTotal.Size = new System.Drawing.Size(39, 13);
            this.labelInfoTotal.TabIndex = 37;
            this.labelInfoTotal.Text = "totalan";
            // 
            // labelTotalHarga
            // 
            this.labelTotalHarga.AutoSize = true;
            this.labelTotalHarga.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelTotalHarga.Location = new System.Drawing.Point(120, 320);
            this.labelTotalHarga.Name = "labelTotalHarga";
            this.labelTotalHarga.Size = new System.Drawing.Size(72, 13);
            this.labelTotalHarga.TabIndex = 36;
            this.labelTotalHarga.Text = "Total Harga : ";
            // 
            // labelInfoPekerjaan
            // 
            this.labelInfoPekerjaan.AutoSize = true;
            this.labelInfoPekerjaan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelInfoPekerjaan.Location = new System.Drawing.Point(194, 280);
            this.labelInfoPekerjaan.Name = "labelInfoPekerjaan";
            this.labelInfoPekerjaan.Size = new System.Drawing.Size(72, 13);
            this.labelInfoPekerjaan.TabIndex = 39;
            this.labelInfoPekerjaan.Text = "Pekerjaannya";
            // 
            // labelPekerjaan
            // 
            this.labelPekerjaan.AutoSize = true;
            this.labelPekerjaan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelPekerjaan.Location = new System.Drawing.Point(128, 280);
            this.labelPekerjaan.Name = "labelPekerjaan";
            this.labelPekerjaan.Size = new System.Drawing.Size(64, 13);
            this.labelPekerjaan.TabIndex = 38;
            this.labelPekerjaan.Text = "Pekerjaan : ";
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonLogin.FlatAppearance.BorderSize = 0;
            this.buttonLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Location = new System.Drawing.Point(363, 88);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(87, 42);
            this.buttonLogin.TabIndex = 41;
            this.buttonLogin.Text = "Cek";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(514, 444);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 41);
            this.button1.TabIndex = 42;
            this.button1.Text = "KEMBALI";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonInputPembayaran
            // 
            this.buttonInputPembayaran.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonInputPembayaran.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonInputPembayaran.FlatAppearance.BorderSize = 0;
            this.buttonInputPembayaran.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonInputPembayaran.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInputPembayaran.ForeColor = System.Drawing.Color.Black;
            this.buttonInputPembayaran.Location = new System.Drawing.Point(458, 152);
            this.buttonInputPembayaran.Name = "buttonInputPembayaran";
            this.buttonInputPembayaran.Size = new System.Drawing.Size(157, 153);
            this.buttonInputPembayaran.TabIndex = 43;
            this.buttonInputPembayaran.Text = "Input Pembayaran";
            this.buttonInputPembayaran.UseVisualStyleBackColor = false;
            this.buttonInputPembayaran.Click += new System.EventHandler(this.buttonInputPembayaran_Click);
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelDate.Location = new System.Drawing.Point(7, 465);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(30, 13);
            this.labelDate.TabIndex = 44;
            this.labelDate.Text = "Date";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormKeuanganTransaksi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProjectBesarSCM.Properties.Resources.Keuangan_Transaksi;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(620, 495);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.buttonInputPembayaran);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.labelInfoPekerjaan);
            this.Controls.Add(this.labelPekerjaan);
            this.Controls.Add(this.labelInfoTotal);
            this.Controls.Add(this.labelTotalHarga);
            this.Controls.Add(this.labelInfoAlamat);
            this.Controls.Add(this.labelAlamat);
            this.Controls.Add(this.labelInfoPemesan);
            this.Controls.Add(this.labelPemesanan);
            this.Controls.Add(this.labelInfoTanggal);
            this.Controls.Add(this.labelTanggal);
            this.Controls.Add(this.textBoxNoSpk);
            this.Controls.Add(this.labelNoSpk);
            this.Controls.Add(this.labelHello);
            this.Name = "FormKeuanganTransaksi";
            this.Text = "FormKeuanganTransaksi";
            this.Load += new System.EventHandler(this.FormKeuanganTransaksi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHello;
        private System.Windows.Forms.Label labelNoSpk;
        private System.Windows.Forms.TextBox textBoxNoSpk;
        private System.Windows.Forms.Label labelTanggal;
        private System.Windows.Forms.Label labelInfoTanggal;
        private System.Windows.Forms.Label labelInfoPemesan;
        private System.Windows.Forms.Label labelPemesanan;
        private System.Windows.Forms.Label labelInfoAlamat;
        private System.Windows.Forms.Label labelAlamat;
        private System.Windows.Forms.Label labelInfoTotal;
        private System.Windows.Forms.Label labelTotalHarga;
        private System.Windows.Forms.Label labelInfoPekerjaan;
        private System.Windows.Forms.Label labelPekerjaan;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonInputPembayaran;
        public System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Timer timer1;
    }
}
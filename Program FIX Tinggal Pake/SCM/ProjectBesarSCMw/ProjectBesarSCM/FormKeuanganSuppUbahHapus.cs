﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ProjectBesarSCM
{
    public partial class FormKeuanganSuppUbahHapus : Form
    {
        public FormKeuanganSuppUbahHapus()
        {
            InitializeComponent();
        }

        private void FormKeuanganSuppUbahHapus_Load(object sender, EventArgs e)
        {
            this.CenterToScreen(); timer1.Start(); ListSupplier ls = new ListSupplier();
            ListBahanBaku lbb = new ListBahanBaku();
            string hasil = lbb.BacaSemuaData();
            string hasilSupplier = ls.BacaSemuaData();
            if (hasilSupplier == "sukses")
            {
                for (int i = 0; i < ls.JumlahList; i++)
                {
                    string kode = ls.LSupplier[i].KodeSup;
                    string nama = ls.LSupplier[i].NamaSup;
                    comboBox1.Items.Add(kode + "-" + nama);
                }
            }
            else
            {
                MessageBox.Show(hasilSupplier);
            }
            if(hasil == "sukses")
            {
                for (int i = 0; i < lbb.JumlahBarang; i++)
                {
                    string nama = lbb.LBahanBaku[i].NamaBarang;
                    
                    comboBox2.Items.Add(nama);
                }
            }
            
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {

        }

        private void buttonSimpan_Click(object sender, EventArgs e)
        {
            string supplier = comboBox1.Text;
            string bahanBaku = comboBox2.Text;

            Koneksi k = new Koneksi();
            k.Connect();
            MySqlCommand cmd = new MySqlCommand("delete from detilbahanbakusupplier WHERE kodeBahanBaku = '" + bahanBaku.ToString().Substring(0, 5) + "' AND kodeSupplier = '" + supplier.ToString().Substring(0, 2) + "'", k.KoneksiDB);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data berhasil dihapus");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelDate.Text = DateTime.Now.ToString("hh:mm:ss");
        }
    }
}

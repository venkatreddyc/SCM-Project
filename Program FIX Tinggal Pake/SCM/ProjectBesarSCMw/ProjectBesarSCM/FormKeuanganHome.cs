﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;
namespace ProjectBesarSCM
{
    public partial class FormKeuanganHome : Form
    {
        public int Status = 0;
        public FormKeuanganHome()
        {
            InitializeComponent();
        }
        public List<string> Nama = new List<string>();
        private void FormKeuanganHome_Load(object sender, EventArgs e)
        {
            this.CenterToScreen(); timer1.Start();
            
            if(Status == 0)
            {

            }
            else
            {
                FormHome FH = (FormHome)this.Owner;
                labelHello.Text = FH.label3.Text;
                    buttonPembelian.Enabled = false;
                    Status = 1;
                
                this.IsMdiContainer = true;
            }
            
            //Form1 F1 = (Form1)this.Owner;

            //Nama.Add(F1.nama);
            //labelHello.Text = "Hello, " + F1.nama + "-" + F1.nama;
          
        }

        private void buttonPembelian_Click(object sender, EventArgs e)
        {
            Form form = Application.OpenForms["formKeuanganPBB"];

            if (form == null)
            {
                formKeuanganPBB FormKeuanganPbb = new formKeuanganPBB();
                FormKeuanganPbb.Owner = this;
                FormKeuanganPbb.Show();
            }
            else
            {
                form.Show();
                form.BringToFront();
            }
        }

        private void buttonTransaksiCustomer_Click(object sender, EventArgs e)
        {
            Form form = Application.OpenForms["FormKeuanganTransaksi"];

            if (form == null)
            {
                FormKeuanganTransaksi formKeuanganTransaksi = new FormKeuanganTransaksi();
                formKeuanganTransaksi.Owner = this;
                formKeuanganTransaksi.Show();
            }
            else
            {
                form.Show();
                form.BringToFront();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelDate.Text = DateTime.Now.ToString("hh:mm:ss");
        }
    }
}

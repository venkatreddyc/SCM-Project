﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;

namespace ProjectBesarSCM
{
    public class ClassKeuanganTransaksi
    {
        private DateTime tanggal;
        private string nama;
        private string alamat;
        private string pekerjaan;
        private int total;
        private string id;

        public DateTime Tanggal
        {
            get { return tanggal; }
            set { tanggal = value; }
        }
        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }
        public string Alamat
        {
            get { return alamat; }
            set { alamat = value; }
        }
        public string Pekerjaan
        {
            get { return pekerjaan; }
            set { pekerjaan = value; }
        }
        public int Total
        {
            get { return total; }
            set { total = value; }
        }
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public ClassKeuanganTransaksi()
        {
            tanggal = new DateTime();
            nama = "";
            alamat = "";
            pekerjaan = "";
            total = 0;
            id = "";
        }
        public ClassKeuanganTransaksi(DateTime Date, string nma, string almt, string pkrjn, int sum, string aidi)
        {
            tanggal = Date;
            nama = nma;
            alamat = almt;
            pekerjaan = pkrjn;
            total = sum;
            id = aidi;
        }
    }
}

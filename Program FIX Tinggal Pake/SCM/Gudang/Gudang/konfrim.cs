﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gudang
{
    public class konfrim
    {
        private string kodeB;
        private string nama;
        private string supplier;
        private int hargaSatuan;
        private int jumlah;
        private int status;
        private DateTime tanggalBeli;
        private DateTime tanggalDatang;




        public string KodeB
        {
            get { return kodeB; }
            set { kodeB = value; }
        }
        public DateTime TanggalDatang
        {
            get { return tanggalDatang; }
            set { tanggalDatang = value; }
        }


        public DateTime TanggalBeli
        {
            get { return tanggalBeli; }
            set { tanggalBeli = value; }
        }

        #region Properties
        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }

        public string Supplier
        {
            get { return supplier; }
            set { supplier = value; }
        }

        public int HargaSatuan
        {
            get { return hargaSatuan; }
            set { hargaSatuan = value; }
        }

        public int Jumlah
        {
            get { return jumlah; }
            set { jumlah = value; }
        }
        public int Status
        {
            get { return status; }
            set { status = value; }
        }


#endregion

        public konfrim()
        {
            kodeB = "";
            nama = "";
            supplier = "";
            hargaSatuan = 0;
            jumlah = 0;
            tanggalBeli = new DateTime();
            status = 0;
            tanggalDatang = new DateTime();
        }

        public konfrim(string kB,string _nama, string Supp, int hargaSat, int jum, DateTime tanggalB, int stat, DateTime tanggalD)
        {
            KodeB = kB;
            Nama = _nama;
            Supplier = Supp;
            HargaSatuan = hargaSat;
            Jumlah = jum;
            TanggalBeli = tanggalB;
            Status = stat;
            TanggalDatang = tanggalD;
        }

    }
}

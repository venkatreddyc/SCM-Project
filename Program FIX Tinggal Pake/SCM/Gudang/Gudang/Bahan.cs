﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gudang
{
    public class Bahan
    {
        private string kodeBahan;
        private string keterangan;
        private string namaBahan;
        private int jumlah;
        private bool status;

        public bool Status
        {
            get { return status; }
            set { status = value; }
        }



        #region Properties
        public string KodeBahan
        {
            get { return kodeBahan; }
            set { kodeBahan = value; }
        }

        public string NamaBahan
        {
            get { return namaBahan; }
            set { namaBahan = value; }
        }
        public int Jumlah
        {
            get { return jumlah; }
            set { jumlah = value; }
        }
        public string Keterangan
        {
            get { return keterangan; }
            set { keterangan = value; }
        }
        #endregion

        #region Contructor

        public Bahan()
        {
            KodeBahan = "";
            NamaBahan = "";
            Jumlah = 0;
            Keterangan = "";
            Status = false;
        }
        public Bahan(string kBahan, string nBahan, int jum, string ket, bool stat)
        {
            KodeBahan = kBahan;
            NamaBahan = nBahan;
            Jumlah = jum;
            Keterangan = ket;
            Status = stat;

        }


        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Gudang
{
    public class DaftarUser
    {
        private List<User> listPegawai;
        private string kodeTerakhir;

        #region Properties
        public List<User> ListPegawai
        {
            get { return listPegawai; }

        }
        public int jumlahList
        {
            get { return listPegawai.Count; }

        }
        public string KodeTerakhir
        {
            get { return kodeTerakhir; }

        }
        #endregion

        #region Constructor
        public DaftarUser()
        {
            listPegawai = new List<User>();
            kodeTerakhir = "1";
        }
        #endregion

        #region Method
        public string beriHakAkses(string username, string namaServer)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "GRANT ALL PRIVILEGES ON *.* TO '" + username + "'@'" + namaServer + "' WITH GRANT OPTION";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MSC.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
        public string BacaSemuaData()
        {
            Koneksi k = new Koneksi();
            k.Connect();
            string sql = "SELECT P.idUser, P.Nama, P.Alamat, P.Telepon, P.Username, P.Password,J.kodejabatan, J.NamaJabatan" +
                " FROM userti P INNER JOIN jabatan J ON P.kodeJabatan = J.kodeJabatan"; //mengisikan perintah sql yg akan dijalankan
            MySqlCommand c = new MySqlCommand(sql, k.KoneksiDB); //membuat mysqlcommandnya
            try
            {
                MySqlDataReader data = c.ExecuteReader(); //gunakan MySqlReader dan ExecuteReader untuk menjalankan perintah SELECT

                while (data.Read() == true) //Selama data reader masih bisa membaca (selama masih ada data)
                {
                    string IdUser = data.GetValue(0).ToString();
                    string NamaUser = data.GetValue(1).ToString();
                    string AlamatUser = data.GetValue(2).ToString();
                    string TeleponUser = data.GetValue(3).ToString();
                    string UsernameUser = data.GetValue(4).ToString();
                    string PasswordUser = data.GetValue(5).ToString();
                    string kodeJabatan = data.GetValue(6).ToString();
                    string namaJabatan = data.GetValue(7).ToString();
                    Jabatan J = new Jabatan(kodeJabatan, namaJabatan);
                    User U = new User(IdUser, NamaUser, AlamatUser, TeleponUser, UsernameUser, PasswordUser, J);
                    listPegawai.Add(U);
                }
                c.Dispose(); //hapus MySqlCommand setelah selesai
                data.Dispose(); //hapus data reader setelah selesai

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string CariData(string kriteria, string nilaiKriteria)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT Distinct P.idUser, P.Nama, P.Alamat, P.Telepon, P.Username, P.Password,J.kodejabatan, J.NamaJabatan" +
                " FROM userti P INNER JOIN jabatan J ON P.kodeJabatan = J.kodeJabatan WHERE " + kriteria + " LIKE '%" + nilaiKriteria + "%'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader data = MSC.ExecuteReader(); //gunakan MySqlReader dan ExecuteReader untuk menjalankan perintah SELECT

                while (data.Read() == true) //Selama data reader masih bisa membaca (selama masih ada data)
                {

                    string IdUser = data.GetValue(0).ToString();
                    string NamaUser = data.GetValue(1).ToString();
                    string AlamatUser = data.GetValue(2).ToString();
                    string TeleponUser = data.GetValue(3).ToString();
                    string UsernameUser = data.GetValue(4).ToString();
                    string PasswordUser = data.GetValue(5).ToString();
                    string kodeJabatan = data.GetValue(6).ToString();
                    string namaJabatan = data.GetValue(7).ToString();
                    Jabatan J = new Jabatan(kodeJabatan, namaJabatan);
                    User U = new User(IdUser, NamaUser, AlamatUser, TeleponUser, UsernameUser, PasswordUser, J);
                    listPegawai.Add(U);
                }
                MSC.Dispose(); //hapus MySqlCommand setelah selesai
                data.Dispose(); //hapus data reader setelah selesai

                return "sukses";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string buatUserBaru(string username, string password, string namaServer)
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "CREATE USER '" + username + "'@'" + namaServer + "' IDENTIFIED BY '" + password + "'";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MSC.ExecuteNonQuery();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
        public string GenerateKode()
        {
            Koneksi K = new Koneksi();
            K.Connect();
            string sql = "SELECT KodePegawai FROM pegawai ORDER BY KodePegawai DESC LIMIT 1";
            MySqlCommand MSC = new MySqlCommand(sql, K.KoneksiDB);
            try
            {
                MySqlDataReader Data = MSC.ExecuteReader();
                if (Data.Read() == true)
                {
                    int kdTerbaru = int.Parse(Data.GetValue(0).ToString()) + 1;
                    kodeTerakhir = kdTerbaru.ToString();
                }
                Data.Dispose();
                MSC.Dispose();
                return "sukses";
            }
            catch (Exception E)
            {
                return E.Message;
            }
        }
        #endregion
    }
}

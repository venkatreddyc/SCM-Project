﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class TambahProgress : Form
    {
        string admin_username, admin_kategori;

        public TambahProgress()
        {
            InitializeComponent();
            FillCombo();
        }
        void FillCombo()
        {
            string loadstring = @"server=localhost;database=scm;userid=root;password=;";
            MySqlConnection conDataBase = new MySqlConnection(loadstring);
            string sql = "SELECT * FROM mesin";
            MySqlConnection MyConn2 = new MySqlConnection(loadstring);
            MySqlCommand MyCommand2 = new MySqlCommand(sql, MyConn2);
            MySqlDataReader MyReader2;
            try
            {
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                    string namamesin =MyReader2.GetString("noMesin")+ "-" + MyReader2.GetString("nama");
                    cbxMesin.Items.Add(namamesin);
                }
                //MessageBox.Show("Import Mesin berhasil");
            }
            catch
            {

            }

        }
        private void btnSimpan_Click(object sender, EventArgs e)
        {

        }

        private void TambahProgress_Load(object sender, EventArgs e)
        {
            timer1_A_FJS.Start();
            Admin_FormHome AFH = (Admin_FormHome)this.Owner;
            label4.Text = AFH.labelUser.Text;
            this.CenterToScreen();
            //this.IsMdiContainer = true;
            // labelDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd/hh:mm:ss");
            //labelUser.Text = FormConnect.username + " - " + FormConnect.kategori_user;
            
            cbxMesin.SelectedIndex = 0;
            var loadstring = @"server=localhost;database=scm;userid=root;password=;";
            using (var connection = new MySqlConnection(loadstring))
            {
                connection.Open();
                var query = "SELECT noSPK, kodeProduk, pekerjaan from spk";
                using (var command1 = new MySqlCommand(query, connection))
                {
                    using (var reader1 = command1.ExecuteReader())
                    {
                        //Iterate through the rows and add it to the combobox's items
                        string a = "";
                        while (reader1.Read())
                        {
                            a = reader1.GetString("noSPK") + "-" + reader1.GetString("pekerjaan");
                            //b = reader1.GetString("noSPK");
                            comboBox1.Items.Add(a);
                            comboBox1.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        private void TambahProgress_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            ProgresProduksi pp = new ProgresProduksi();
            pp.ShowDialog();
        }

        private void btnSimpan_Click_1(object sender, EventArgs e)
        {
            string[] concatNoSPK = comboBox1.SelectedItem.ToString().Split('-');
            string noSPK = concatNoSPK[0];
            //MessageBox.Show(noSPK);


            string[] concatNoMesin = cbxMesin.SelectedItem.ToString().Split('-');
            string noMesin = concatNoMesin[0];
            //MessageBox.Show(noMesin);

            try
            {
                string loadstring = @"server=localhost;database=scm;userid=root;password=;";
                MySqlConnection conDataBase = new MySqlConnection(loadstring);
                string sql = "INSERT INTO progressproduksi (noMesin, noSPK, tanggal_proses, namaPekerjaan, jamKerja, Hasil) VALUES ('" + noMesin + "','" + noSPK + "','" + this.dateTimePicker1.Value.ToString() + "','" + this.txtNama.Text + "','" + this.txtJam.Text + "','" + this.txtHasil.Text + "');";
                MySqlConnection MyConn2 = new MySqlConnection(loadstring);
                MySqlCommand MyCommand2 = new MySqlCommand(sql, MyConn2);
                MySqlDataReader MyReader2;
                MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                MessageBox.Show("Progress BERHASIL ditambahkan");


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void timer1_A_FJS_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            labelDate.Text = dt.ToString("hh:MM:ss"); 
        }
    }
}

﻿namespace Admin
{
    partial class Admin_FormHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelUser = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.buttonSPK = new System.Windows.Forms.Button();
            this.buttonPesanBahanBaku = new System.Windows.Forms.Button();
            this.buttonPenggunaan = new System.Windows.Forms.Button();
            this.buttonJadwalProduksi = new System.Windows.Forms.Button();
            this.buttonProgress = new System.Windows.Forms.Button();
            this.buttonPengiriman = new System.Windows.Forms.Button();
            this.buttonPenerimaan = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1_A_FJS = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelUser.Font = new System.Drawing.Font("Schadow BT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Location = new System.Drawing.Point(501, 9);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(41, 19);
            this.labelUser.TabIndex = 0;
            this.labelUser.Text = "User";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelDate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelDate.Location = new System.Drawing.Point(4, 395);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(58, 13);
            this.labelDate.TabIndex = 1;
            this.labelDate.Text = "Date/Time";
            // 
            // buttonSPK
            // 
            this.buttonSPK.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonSPK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSPK.FlatAppearance.BorderSize = 0;
            this.buttonSPK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonSPK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSPK.Location = new System.Drawing.Point(187, 94);
            this.buttonSPK.Name = "buttonSPK";
            this.buttonSPK.Size = new System.Drawing.Size(163, 108);
            this.buttonSPK.TabIndex = 2;
            this.buttonSPK.Text = "SURAT PERINTAH KERJA";
            this.buttonSPK.UseVisualStyleBackColor = false;
            this.buttonSPK.Click += new System.EventHandler(this.buttonSPK_Click);
            // 
            // buttonPesanBahanBaku
            // 
            this.buttonPesanBahanBaku.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPesanBahanBaku.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPesanBahanBaku.FlatAppearance.BorderSize = 0;
            this.buttonPesanBahanBaku.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonPesanBahanBaku.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPesanBahanBaku.Location = new System.Drawing.Point(363, 94);
            this.buttonPesanBahanBaku.Name = "buttonPesanBahanBaku";
            this.buttonPesanBahanBaku.Size = new System.Drawing.Size(162, 108);
            this.buttonPesanBahanBaku.TabIndex = 3;
            this.buttonPesanBahanBaku.Text = "PESAN BAHAN BAKU";
            this.buttonPesanBahanBaku.UseVisualStyleBackColor = false;
            this.buttonPesanBahanBaku.Click += new System.EventHandler(this.buttonPesanBahanBaku_Click);
            // 
            // buttonPenggunaan
            // 
            this.buttonPenggunaan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPenggunaan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPenggunaan.FlatAppearance.BorderSize = 0;
            this.buttonPenggunaan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonPenggunaan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPenggunaan.Location = new System.Drawing.Point(12, 94);
            this.buttonPenggunaan.Name = "buttonPenggunaan";
            this.buttonPenggunaan.Size = new System.Drawing.Size(163, 108);
            this.buttonPenggunaan.TabIndex = 4;
            this.buttonPenggunaan.Text = "PENGGUNAAN BAHAN BAKU";
            this.buttonPenggunaan.UseVisualStyleBackColor = false;
            this.buttonPenggunaan.Click += new System.EventHandler(this.buttonPenggunaan_Click);
            // 
            // buttonJadwalProduksi
            // 
            this.buttonJadwalProduksi.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonJadwalProduksi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonJadwalProduksi.FlatAppearance.BorderSize = 0;
            this.buttonJadwalProduksi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonJadwalProduksi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonJadwalProduksi.Location = new System.Drawing.Point(536, 94);
            this.buttonJadwalProduksi.Name = "buttonJadwalProduksi";
            this.buttonJadwalProduksi.Size = new System.Drawing.Size(165, 108);
            this.buttonJadwalProduksi.TabIndex = 5;
            this.buttonJadwalProduksi.Text = "JADWAL PRODUKSI";
            this.buttonJadwalProduksi.UseVisualStyleBackColor = false;
            this.buttonJadwalProduksi.Click += new System.EventHandler(this.buttonJadwalProduksi_Click);
            // 
            // buttonProgress
            // 
            this.buttonProgress.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonProgress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonProgress.FlatAppearance.BorderSize = 0;
            this.buttonProgress.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonProgress.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonProgress.Location = new System.Drawing.Point(278, 224);
            this.buttonProgress.Name = "buttonProgress";
            this.buttonProgress.Size = new System.Drawing.Size(163, 109);
            this.buttonProgress.TabIndex = 6;
            this.buttonProgress.Text = "PROGRESS PRODUKSI";
            this.buttonProgress.UseVisualStyleBackColor = false;
            this.buttonProgress.Click += new System.EventHandler(this.buttonProgress_Click);
            // 
            // buttonPengiriman
            // 
            this.buttonPengiriman.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPengiriman.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPengiriman.FlatAppearance.BorderSize = 0;
            this.buttonPengiriman.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonPengiriman.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPengiriman.Location = new System.Drawing.Point(456, 224);
            this.buttonPengiriman.Name = "buttonPengiriman";
            this.buttonPengiriman.Size = new System.Drawing.Size(160, 55);
            this.buttonPengiriman.TabIndex = 7;
            this.buttonPengiriman.Text = "PENGIRIMAN BARANG JADI";
            this.buttonPengiriman.UseVisualStyleBackColor = false;
            this.buttonPengiriman.Click += new System.EventHandler(this.buttonPengiriman_Click);
            // 
            // buttonPenerimaan
            // 
            this.buttonPenerimaan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPenerimaan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPenerimaan.FlatAppearance.BorderSize = 0;
            this.buttonPenerimaan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonPenerimaan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPenerimaan.Location = new System.Drawing.Point(456, 278);
            this.buttonPenerimaan.Name = "buttonPenerimaan";
            this.buttonPenerimaan.Size = new System.Drawing.Size(160, 54);
            this.buttonPenerimaan.TabIndex = 8;
            this.buttonPenerimaan.Text = "PENERIMAAN BARANG JADI";
            this.buttonPenerimaan.UseVisualStyleBackColor = false;
            this.buttonPenerimaan.Click += new System.EventHandler(this.buttonPenerimaan_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(249, 368);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(350, 45);
            this.button2.TabIndex = 13;
            this.button2.Text = "TAMBAH MESIN";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(102, 224);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 109);
            this.button1.TabIndex = 12;
            this.button1.Text = "TAMBAH BAHAN BAKU";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer1_A_FJS
            // 
            this.timer1_A_FJS.Tick += new System.EventHandler(this.timer1_A_FJS_Tick);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Admin_FormHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin.Properties.Resources.Admin_Home;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(713, 425);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonPenerimaan);
            this.Controls.Add(this.buttonPengiriman);
            this.Controls.Add(this.buttonProgress);
            this.Controls.Add(this.buttonJadwalProduksi);
            this.Controls.Add(this.buttonPenggunaan);
            this.Controls.Add(this.buttonPesanBahanBaku);
            this.Controls.Add(this.buttonSPK);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.labelUser);
            this.Name = "Admin_FormHome";
            this.Text = "FormAdmin_Home";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAdmin_Home_FormClosing);
            this.Load += new System.EventHandler(this.FormAdmin_Home_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Button buttonSPK;
        private System.Windows.Forms.Button buttonPesanBahanBaku;
        private System.Windows.Forms.Button buttonPenggunaan;
        private System.Windows.Forms.Button buttonJadwalProduksi;
        private System.Windows.Forms.Button buttonProgress;
        private System.Windows.Forms.Button buttonPengiriman;
        private System.Windows.Forms.Button buttonPenerimaan;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Timer timer1_A_FJS;
        private System.Windows.Forms.Timer timer1;
    }
}
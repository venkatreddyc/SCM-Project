﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class Admin_FormTambahMesin : Form
    {

        public Admin_FormTambahMesin()
        {
            InitializeComponent();
            timer1_A_FTM.Start();
        }

        private void btnKembali_A_FTM_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSimpan_A_FTM_Click(object sender, EventArgs e)
        {
            if (txtNoMesin_A_FTM.Text.Length > 3)
            {
                MessageBox.Show("Nama mesin maksimal 3 character");
            }
            else
            {
                Mesin m = new Mesin(txtNoMesin_A_FTM.Text,txtNamaMesin_A_FTM.Text);
                string hasil = m.tambahMesin();

                if(hasil == "sukses")
                {
                    MessageBox.Show("Mesin berhasil ditambah");
                }else
                {
                    MessageBox.Show("Mesin gagal ditambah, error : " + hasil);
                }
            }
        }

        private void Admin_FormTambahMesin_Load(object sender, EventArgs e)
        {
            this.CenterToScreen(); timer1_A_FTM.Start(); DateTime dt = DateTime.Now;
            lblDateTime_A_FTM.Text = dt.ToString("hh:MM:ss");
            Admin_FormHome AFH = (Admin_FormHome)this.Owner;
            lblUser_A_FTM.Text  = AFH.labelUser.Text;
        }

        private void timer1_A_FTM_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            lblDateTime_A_FTM.Text = dt.ToString("hh:MM:ss");
        }
    }
}

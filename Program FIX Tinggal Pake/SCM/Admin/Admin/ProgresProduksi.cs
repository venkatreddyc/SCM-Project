﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class ProgresProduksi : Form
    {
        string username, kategori;
        public ProgresProduksi()
        {
            InitializeComponent();
        }

        private void buttonPenerimaan_Click(object sender, EventArgs e)
        {
            RiwayatProgres ri = new RiwayatProgres();
            ri.ShowDialog();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ProgresProduksi_Load(object sender, EventArgs e)
        {
            timer1_A_FJS.Start(); DateTime dt = DateTime.Now;
            labelDate.Text = dt.ToString("hh:MM:ss");
            this.CenterToScreen();
            Admin_FormHome fh = (Admin_FormHome)this.Owner;
            if(fh.status == 1)
            {
                buttonPengiriman.Enabled = false;
            }
            label2.Text = fh.labelUser.Text;
        //    label2.Text = username + " - " + kategori;
        }

        private void buttonPengiriman_Click(object sender, EventArgs e)
        {
            this.Hide();
            TambahProgress tp = new TambahProgress();
            tp.ShowDialog();
        }

        private void timer1_A_FJS_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            labelDate.Text = dt.ToString("hh:MM:ss"); 
        }
    }
}

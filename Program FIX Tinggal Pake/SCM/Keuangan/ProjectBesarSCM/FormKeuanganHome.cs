﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace ProjectBesarSCM
{
    public partial class FormKeuanganHome : Form
    {
        public FormKeuanganHome()
        {
            InitializeComponent();
        }
        public List<string> Nama = new List<string>();
        private void FormKeuanganHome_Load(object sender, EventArgs e)
        {
            //this.IsMdiContainer = true;
            string label = "";
            //FormHome FH = (FormHome)this.Owner;
            //labelHello.Text = FH.Nama;
            label = labelHello.Text;
            string Jam = DateTime.Now.ToString("dd-MM-yyyy");
            string Waktu = DateTime.Now.ToString("HH:mm");
            labelWaktu.Text = Waktu;
            labelJam.Text = Jam;
          
        }

        private void buttonPembelian_Click(object sender, EventArgs e)
        {
            formKeuanganPBB FormKeuanganPbb = new formKeuanganPBB();
            FormKeuanganPbb.Owner = this;
            FormKeuanganPbb.Show();
        }
    }
}

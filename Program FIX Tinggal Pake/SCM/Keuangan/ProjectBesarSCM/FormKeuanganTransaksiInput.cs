﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectBesarSCM
{
    public partial class FormKeuanganTransaksiInput : Form
    {
        public FormKeuanganTransaksiInput()
        {
            InitializeComponent();
        }

        private void FormKeuanganTransaksiInput_Load(object sender, EventArgs e)
        {
            string Jam = DateTime.Now.ToString("dd-MM-yyyy");
            string Waktu = DateTime.Now.ToString("HH:mm");
            labelWaktu.Text = Waktu;
            labelJam.Text = Jam;
        }
    }
}

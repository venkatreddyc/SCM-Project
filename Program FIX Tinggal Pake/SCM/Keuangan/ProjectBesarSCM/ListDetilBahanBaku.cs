﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ProjectBesarSCM
{
    class ListDetilBahanBaku
    {
        private List<DetilBahanBaku> listDetilBahanBaku;
        private int kode;
        private string noNotaTerbaru;
        public List<DetilBahanBaku> LIstDetilBahanBaku
        {
            get { return listDetilBahanBaku; }
        }
        public int Kode
        {
            get { return kode; }
        }
        public int JumlahList
        {
            get { return listDetilBahanBaku.Count; }
        }
        public string NoNotaTerbaru
        {
            get { return noNotaTerbaru; }
        }

        public ListDetilBahanBaku()
        {
            listDetilBahanBaku = new List<DetilBahanBaku>();
            kode = 1;
            noNotaTerbaru = "20170101001";
        }

    }
}

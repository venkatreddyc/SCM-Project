﻿namespace ProjectBesarSCM
{
    partial class FormKeuanganTransaksi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormKeuanganTransaksi));
            this.labelHello = new System.Windows.Forms.Label();
            this.labelNoSpk = new System.Windows.Forms.Label();
            this.textBoxNoSpk = new System.Windows.Forms.TextBox();
            this.labelJam = new System.Windows.Forms.Label();
            this.labelWaktu = new System.Windows.Forms.Label();
            this.labelTanggal = new System.Windows.Forms.Label();
            this.labelInfoTanggal = new System.Windows.Forms.Label();
            this.labelInfoPemesan = new System.Windows.Forms.Label();
            this.labelPemesanan = new System.Windows.Forms.Label();
            this.labelInfoAlamat = new System.Windows.Forms.Label();
            this.labelAlamat = new System.Windows.Forms.Label();
            this.labelInfoTotal = new System.Windows.Forms.Label();
            this.labelTotalHarga = new System.Windows.Forms.Label();
            this.labelInfoPekerjaan = new System.Windows.Forms.Label();
            this.labelPekerjaan = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelHello
            // 
            this.labelHello.AutoSize = true;
            this.labelHello.Location = new System.Drawing.Point(439, 15);
            this.labelHello.Name = "labelHello";
            this.labelHello.Size = new System.Drawing.Size(43, 13);
            this.labelHello.TabIndex = 20;
            this.labelHello.Text = "Hello  , ";
            // 
            // labelNoSpk
            // 
            this.labelNoSpk.AutoSize = true;
            this.labelNoSpk.Location = new System.Drawing.Point(120, 103);
            this.labelNoSpk.Name = "labelNoSpk";
            this.labelNoSpk.Size = new System.Drawing.Size(53, 13);
            this.labelNoSpk.TabIndex = 21;
            this.labelNoSpk.Text = "NO. SPK:";
            // 
            // textBoxNoSpk
            // 
            this.textBoxNoSpk.Location = new System.Drawing.Point(172, 100);
            this.textBoxNoSpk.Name = "textBoxNoSpk";
            this.textBoxNoSpk.Size = new System.Drawing.Size(185, 20);
            this.textBoxNoSpk.TabIndex = 22;
            // 
            // labelJam
            // 
            this.labelJam.AutoSize = true;
            this.labelJam.Location = new System.Drawing.Point(9, 457);
            this.labelJam.Name = "labelJam";
            this.labelJam.Size = new System.Drawing.Size(26, 13);
            this.labelJam.TabIndex = 29;
            this.labelJam.Text = "Jam";
            // 
            // labelWaktu
            // 
            this.labelWaktu.AutoSize = true;
            this.labelWaktu.Location = new System.Drawing.Point(9, 472);
            this.labelWaktu.Name = "labelWaktu";
            this.labelWaktu.Size = new System.Drawing.Size(39, 13);
            this.labelWaktu.TabIndex = 28;
            this.labelWaktu.Text = "Waktu";
            // 
            // labelTanggal
            // 
            this.labelTanggal.AutoSize = true;
            this.labelTanggal.Location = new System.Drawing.Point(150, 188);
            this.labelTanggal.Name = "labelTanggal";
            this.labelTanggal.Size = new System.Drawing.Size(55, 13);
            this.labelTanggal.TabIndex = 30;
            this.labelTanggal.Text = "Tanggal : ";
            // 
            // labelInfoTanggal
            // 
            this.labelInfoTanggal.AutoSize = true;
            this.labelInfoTanggal.Location = new System.Drawing.Point(208, 188);
            this.labelInfoTanggal.Name = "labelInfoTanggal";
            this.labelInfoTanggal.Size = new System.Drawing.Size(46, 13);
            this.labelInfoTanggal.TabIndex = 31;
            this.labelInfoTanggal.Text = "Tanggal";
            // 
            // labelInfoPemesan
            // 
            this.labelInfoPemesan.AutoSize = true;
            this.labelInfoPemesan.Location = new System.Drawing.Point(207, 215);
            this.labelInfoPemesan.Name = "labelInfoPemesan";
            this.labelInfoPemesan.Size = new System.Drawing.Size(78, 13);
            this.labelInfoPemesan.TabIndex = 33;
            this.labelInfoPemesan.Text = " Nama Pembeli";
            // 
            // labelPemesanan
            // 
            this.labelPemesanan.AutoSize = true;
            this.labelPemesanan.Location = new System.Drawing.Point(145, 215);
            this.labelPemesanan.Name = "labelPemesanan";
            this.labelPemesanan.Size = new System.Drawing.Size(60, 13);
            this.labelPemesanan.TabIndex = 32;
            this.labelPemesanan.Text = "Pemesan : ";
            // 
            // labelInfoAlamat
            // 
            this.labelInfoAlamat.AutoSize = true;
            this.labelInfoAlamat.Location = new System.Drawing.Point(207, 239);
            this.labelInfoAlamat.Name = "labelInfoAlamat";
            this.labelInfoAlamat.Size = new System.Drawing.Size(79, 13);
            this.labelInfoAlamat.TabIndex = 35;
            this.labelInfoAlamat.Text = "Alamat Pembeli";
            // 
            // labelAlamat
            // 
            this.labelAlamat.AutoSize = true;
            this.labelAlamat.Location = new System.Drawing.Point(157, 239);
            this.labelAlamat.Name = "labelAlamat";
            this.labelAlamat.Size = new System.Drawing.Size(48, 13);
            this.labelAlamat.TabIndex = 34;
            this.labelAlamat.Text = "Alamat : ";
            // 
            // labelInfoTotal
            // 
            this.labelInfoTotal.AutoSize = true;
            this.labelInfoTotal.Location = new System.Drawing.Point(208, 303);
            this.labelInfoTotal.Name = "labelInfoTotal";
            this.labelInfoTotal.Size = new System.Drawing.Size(39, 13);
            this.labelInfoTotal.TabIndex = 37;
            this.labelInfoTotal.Text = "totalan";
            // 
            // labelTotalHarga
            // 
            this.labelTotalHarga.AutoSize = true;
            this.labelTotalHarga.Location = new System.Drawing.Point(133, 303);
            this.labelTotalHarga.Name = "labelTotalHarga";
            this.labelTotalHarga.Size = new System.Drawing.Size(72, 13);
            this.labelTotalHarga.TabIndex = 36;
            this.labelTotalHarga.Text = "Total Harga : ";
            // 
            // labelInfoPekerjaan
            // 
            this.labelInfoPekerjaan.AutoSize = true;
            this.labelInfoPekerjaan.Location = new System.Drawing.Point(207, 263);
            this.labelInfoPekerjaan.Name = "labelInfoPekerjaan";
            this.labelInfoPekerjaan.Size = new System.Drawing.Size(72, 13);
            this.labelInfoPekerjaan.TabIndex = 39;
            this.labelInfoPekerjaan.Text = "Pekerjaannya";
            // 
            // labelPekerjaan
            // 
            this.labelPekerjaan.AutoSize = true;
            this.labelPekerjaan.Location = new System.Drawing.Point(141, 263);
            this.labelPekerjaan.Name = "labelPekerjaan";
            this.labelPekerjaan.Size = new System.Drawing.Size(64, 13);
            this.labelPekerjaan.TabIndex = 38;
            this.labelPekerjaan.Text = "Pekerjaan : ";
            // 
            // FormKeuanganTransaksi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(620, 495);
            this.Controls.Add(this.labelInfoPekerjaan);
            this.Controls.Add(this.labelPekerjaan);
            this.Controls.Add(this.labelInfoTotal);
            this.Controls.Add(this.labelTotalHarga);
            this.Controls.Add(this.labelInfoAlamat);
            this.Controls.Add(this.labelAlamat);
            this.Controls.Add(this.labelInfoPemesan);
            this.Controls.Add(this.labelPemesanan);
            this.Controls.Add(this.labelInfoTanggal);
            this.Controls.Add(this.labelTanggal);
            this.Controls.Add(this.labelJam);
            this.Controls.Add(this.labelWaktu);
            this.Controls.Add(this.textBoxNoSpk);
            this.Controls.Add(this.labelNoSpk);
            this.Controls.Add(this.labelHello);
            this.Name = "FormKeuanganTransaksi";
            this.Text = "FormKeuanganTransaksi";
            this.Load += new System.EventHandler(this.FormKeuanganTransaksi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHello;
        private System.Windows.Forms.Label labelNoSpk;
        private System.Windows.Forms.TextBox textBoxNoSpk;
        private System.Windows.Forms.Label labelJam;
        private System.Windows.Forms.Label labelWaktu;
        private System.Windows.Forms.Label labelTanggal;
        private System.Windows.Forms.Label labelInfoTanggal;
        private System.Windows.Forms.Label labelInfoPemesan;
        private System.Windows.Forms.Label labelPemesanan;
        private System.Windows.Forms.Label labelInfoAlamat;
        private System.Windows.Forms.Label labelAlamat;
        private System.Windows.Forms.Label labelInfoTotal;
        private System.Windows.Forms.Label labelTotalHarga;
        private System.Windows.Forms.Label labelInfoPekerjaan;
        private System.Windows.Forms.Label labelPekerjaan;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gudang;
using ProjectBesarSCM;
using Admin;

namespace WindowsFormsApplication1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void panelPengaturan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
           Koneksi k = new Koneksi(textBoxServer.Text, textBoxDataBase.Text, textBoxUsername.Text, textBoxPass.Text);
           string hasil = k.Connect();

          // MessageBox.Show("Selamat Datang Di Sistem");
           LUser listUser = new LUser();
           string hasilBaca = listUser.CariData("username", textBoxUsername.Text);
           if (hasilBaca == "sukses")
           {
               
               string jabat = "";
               jabat =  listUser.ListPegawai[0].JabatanUser.NamaJabatan;
               this.Hide();
               if(jabat == "Owner")
               {
                   FormHome FH = new FormHome();
                   FH.status = 1;
                   FH.Status2 = 1;
                   FH.label3.Text = "Hallo, " + listUser.ListPegawai[0].Nama + " - " + listUser.ListPegawai[0].JabatanUser.NamaJabatan;
                   FH.ShowDialog();
                   this.Show();
               }
               else if(jabat == "Admin")
               {
                   Admin_FormHome afh = new Admin_FormHome();

                   afh.Owner = this;
                   afh.Show();
                   afh.labelUser.Text = "Hallo, " + listUser.ListPegawai[0].Nama + " - " + listUser.ListPegawai[0].JabatanUser.NamaJabatan;
               }
               else if (jabat == "Finance")
               {
                   FormKeuanganHome fkh = new FormKeuanganHome();
                   fkh.Owner = this;
                   fkh.Show();
                   fkh.labelHello.Text = "Hallo, " + listUser.ListPegawai[0].Nama + " - " + listUser.ListPegawai[0].JabatanUser.NamaJabatan;
                   this.Show();
               }
               else if (jabat == "Gudang")
               {
                   FormUtama fu = new FormUtama();
                  
                   FormDaftarBarang fdb = new FormDaftarBarang();
                   FormKonfrimasi fk = new FormKonfrimasi();
                   FormStock fs = new FormStock();
                   Laporan l = new Laporan();
                   fu.Owner = this;
                   fu.Show();
                   fu.labelJabat.Text = "Hallo, " + listUser.ListPegawai[0].Nama + " - " + listUser.ListPegawai[0].JabatanUser.NamaJabatan;
                   this.Show();
               }
           }
           else
           {
               MessageBox.Show("Error" + hasilBaca);
           }
           
            //listUser.BacaSemuaData();
            //if (textBoxUsername.Text != "")
            //{
            //    Koneksi k = new Koneksi(textBoxServer.Text, textBoxDataBase.Text, textBoxUsername.Text, textBoxPass.Text);

            //    string hasilConnect = k.Connect();

            //    if (hasilConnect == "sukses")
            //    {

            //        MessageBox.Show("Selamat Datang di sistem", "Info");
            //        FormHome FH = (FormHome)this.Owner;
                    
                   
            //        FH.Enabled = true;
            //        this.Close();

            //        //if (hasil == "sukses")
            //        //{

            //        //    FU.labelJabatan.Text = DP.ListPegawai[0].JabatanPegawai.KodeJabatan;
            //        //    FU.labelNamaP.Text = DP.ListPegawai[0].NamaPegawai;
            //        //    FU.labelKodePegawai.Text = DP.ListPegawai[0].KodePegawai.ToString();
            //        //PengaturanHakAksesMenu(DP.ListPegawai[0].JabatanPegawai.NamaJabatan);

            //        //}
            //    }
            //    //else
            //    //{
            //    //    MessageBox.Show("Koneksi gagal, Pesan Kesalahan : " + hasilConnect, "Kesalahan");
            //    //}
            //}
            //else
            //{
            //    MessageBox.Show("Username tidak boleh dikosongi", "Kesalahan");
            //}
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            this.Height = 125 + panelLogin.Height;
            this.Width = 32 + panelLogin.Width;

            textBoxServer.Text = "localhost";
            textBoxDataBase.Text = "scm";
            textBoxUsername.Focus();
        }

        private void buttonKeluar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBoxUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonSimpan_Click(object sender, EventArgs e)
        {

        }

        
    }
}

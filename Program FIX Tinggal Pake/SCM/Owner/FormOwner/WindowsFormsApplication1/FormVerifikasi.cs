﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FormVerifikasi : Form
    {
        public FormVerifikasi()
        {
            InitializeComponent();
        }

        int status = 0;

        private void buttonTambahKomentar_Click(object sender, EventArgs e)
        {
            FormKomentar formkomentar = new FormKomentar(comboBoxSpk.Text);
            comboBoxSpk.Text = formkomentar.dariFormVeri;
            formkomentar.Owner = this;
            formkomentar.ShowDialog();
            
            
        }

        private void FormVerifikasi_Load(object sender, EventArgs e)
        {
            this.CenterToScreen(); timer1.Start();
           // label4.Text = DateTime.Now.ToString("hh:MM");
          //  label5.Text = DateTime.Now.ToString("dd/MM/yyyy");
            LUser listUser = new LUser();
            FormHome fh = (FormHome)this.Owner;
            string hasilBaca = listUser.CariData("username", "fleming");
            if (hasilBaca == "sukses")
            {
                label3.Text = fh.label3.Text;
                status = 1;
            }
            else
            {
                MessageBox.Show(hasilBaca);
            }
            DaftarSpk dSpk = new DaftarSpk();
            string hasil = dSpk.bacaData();
            

            if (hasil == "sukses")
            {
                
                 
                for (int i = 0; i < dSpk.jumlahData; i++)
                {
                    int stat = dSpk.ListSpk[i].Status;
                    comboBoxSpk.Items.Add(dSpk.ListSpk[i].NoSpk);
                    if (stat == 1)
                    {
                        string stats0 = "SUDAH DIVERIFIKASI";
                        int status = dSpk.ListSpk[i].Status;
                        string namaPembeli = dSpk.ListSpk[i].NamaPembeli;
                        string alamatPembeli = dSpk.ListSpk[i].AlamatPembeli;
                        string namaUser = dSpk.ListSpk[i].NamaUser;
                        string alamatUser = dSpk.ListSpk[i].AlamatUser;
                        string telepon = dSpk.ListSpk[i].Telepon;
                        string pekerjaan = dSpk.ListSpk[i].Pekerjaan;
                        int biaya = dSpk.ListSpk[i].Biaya;
                        int lama = dSpk.ListSpk[i].Lama;
                        listBoxInfo.Items.Add("'" + stats0 + "'");
                        listBoxInfo.Items.Add("");
                        listBoxInfo.Items.Add("Nama Pembeli : " + namaPembeli);
                        listBoxInfo.Items.Add("Alamat Pembeli : " + alamatPembeli);
                        listBoxInfo.Items.Add("Nama Penanggung Jawab : " + namaUser);
                        listBoxInfo.Items.Add("Alamat Penanggung Jawab: " + alamatUser);
                        listBoxInfo.Items.Add("Telepeon Penanggung Jawab : " + telepon);
                        listBoxInfo.Items.Add("Pekerjaan : " + pekerjaan);
                        listBoxInfo.Items.Add("Biaya : " + biaya);
                        listBoxInfo.Items.Add("Lama : " + lama);
                        listBoxInfo.Items.Add("");
                    }
                    else if (stat == 0)
                    {

                        string stats = "BELUM DIVERIFIKASI";
                        int status = dSpk.ListSpk[i].Status;
                        string namaPembeli = dSpk.ListSpk[i].NamaPembeli;
                        string alamatPembeli = dSpk.ListSpk[i].AlamatPembeli;
                        string namaUser = dSpk.ListSpk[i].NamaUser;
                        string alamatUser = dSpk.ListSpk[i].AlamatUser;
                        string telepon = dSpk.ListSpk[i].Telepon;
                        string pekerjaan = dSpk.ListSpk[i].Pekerjaan;
                        int biaya = dSpk.ListSpk[i].Biaya;
                        int lama = dSpk.ListSpk[i].Lama;
                        listBoxInfo.Items.Add("'" + stats + "'");
                        listBoxInfo.Items.Add("");
                        listBoxInfo.Items.Add("Nama Pembeli : " + namaPembeli);
                        listBoxInfo.Items.Add("Alamat Pembeli : " + alamatPembeli);
                        listBoxInfo.Items.Add("Nama Penanggung Jawab : " + namaUser);
                        listBoxInfo.Items.Add("Alamat Penanggung Jawab: " + alamatUser);
                        listBoxInfo.Items.Add("Telepeon Penanggung Jawab : " + telepon);
                        listBoxInfo.Items.Add("Pekerjaan : " + pekerjaan);
                        listBoxInfo.Items.Add("Biaya : " + biaya);
                        listBoxInfo.Items.Add("Lama : " + lama);
                        listBoxInfo.Items.Add("");
                    }

                }
            }
            else
            {
                MessageBox.Show("error " + hasil);
            }
        }

        private void comboBoxSpk_SelectedIndexChanged(object sender, EventArgs e)
        {
            string noSp = comboBoxSpk.SelectedItem.ToString();
            DaftarSpk dSpk = new DaftarSpk();
            string hasil = dSpk.CariData(noSp);
            int stat = 0;

            if (hasil == "sukses")
            {
                int stats = dSpk.ListSpk[0].Status;
                listBoxInfo.Items.Clear();
                if (stats == 1)
                {
                    string stats0 = "SUDAH DIVERIFIKASI";
                    int status = dSpk.ListSpk[0].Status;
                    string namaPembeli = dSpk.ListSpk[0].NamaPembeli;
                    string alamatPembeli = dSpk.ListSpk[0].AlamatPembeli;
                    string namaUser = dSpk.ListSpk[0].NamaUser;
                    string alamatUser = dSpk.ListSpk[0].AlamatUser;
                    string telepon = dSpk.ListSpk[0].Telepon;
                    string pekerjaan = dSpk.ListSpk[0].Pekerjaan;
                    int biaya = dSpk.ListSpk[0].Biaya;
                    int lama = dSpk.ListSpk[0].Lama;
                    listBoxInfo.Items.Add("'" + stats0 + "'");
                    listBoxInfo.Items.Add("");
                    listBoxInfo.Items.Add("Nama Pembeli : " + namaPembeli);
                    listBoxInfo.Items.Add("Alamat Pembeli : " + alamatPembeli);
                    listBoxInfo.Items.Add("Nama Penanggung Jawab : " + namaUser);
                    listBoxInfo.Items.Add("Alamat Penanggung Jawab: " + alamatUser);
                    listBoxInfo.Items.Add("Telepon Penanggung Jawab : " + telepon);
                    listBoxInfo.Items.Add("Pekerjaan : " + pekerjaan);
                    listBoxInfo.Items.Add("Biaya : " + biaya);
                    listBoxInfo.Items.Add("Lama : " + lama);
                    listBoxInfo.Items.Add("");
                }
                else if (stats == 0)
                {

                    string stat1 = "BELUM DIVERIFIKASI";
                    int status = dSpk.ListSpk[0].Status;
                    string namaPembeli = dSpk.ListSpk[0].NamaPembeli;
                    string alamatPembeli = dSpk.ListSpk[0].AlamatPembeli;
                    string namaUser = dSpk.ListSpk[0].NamaUser;
                    string alamatUser = dSpk.ListSpk[0].AlamatUser;
                    string telepon = dSpk.ListSpk[0].Telepon;
                    string pekerjaan = dSpk.ListSpk[0].Pekerjaan;
                    int biaya = dSpk.ListSpk[0].Biaya;
                    int lama = dSpk.ListSpk[0].Lama;
                    listBoxInfo.Items.Add("'" + stat1 + "'");
                    listBoxInfo.Items.Add("");
                    listBoxInfo.Items.Add("Nama Pembeli : " + namaPembeli);
                    listBoxInfo.Items.Add("Alamat Pembeli : " + alamatPembeli);
                    listBoxInfo.Items.Add("Nama Penanggung Jawab : " + namaUser);
                    listBoxInfo.Items.Add("Alamat Penanggung Jawab: " + alamatUser);
                    listBoxInfo.Items.Add("Telepeon Penanggung Jawab : " + telepon);
                    listBoxInfo.Items.Add("Pekerjaan : " + pekerjaan);
                    listBoxInfo.Items.Add("Biaya : " + biaya);
                    listBoxInfo.Items.Add("Lama : " + lama);
                    listBoxInfo.Items.Add("");
                }
            }
            else
            {
                MessageBox.Show(hasil);
            }
        }

        private void buttonKembali_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime D = DateTime.Now;
            labelDate.Text = D.ToString("hh:MM:ss");
        }
    }
}
